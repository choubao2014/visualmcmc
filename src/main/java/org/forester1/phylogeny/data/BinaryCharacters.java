// $Id:
// FORESTER -- software libraries and applications
// for evolutionary biology research and applications.
//
// Copyright (C) 2008-2009 Christian M. Zmasek
// Copyright (C) 2008-2009 Burnham Institute for Medical Research
// All rights reserved
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
//
// Contact: phylosoft @ gmail . com
// WWW: https://sites.google.com/site/cmzmasek/home/software/forester

package org.forester1.phylogeny.data;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.forester1.util.ForesterUtil;

public class BinaryCharacters implements PhylogenyData {

    public final static int         COUNT_DEFAULT = -1;
    private final SortedSet<String> _present;
    private final SortedSet<String> _gained;
    private final SortedSet<String> _lost;
    private final int               _present_count;
    private final int               _gained_count;
    private final int               _lost_count;
    private String                  _type;

    public BinaryCharacters() {
        _present = new TreeSet<String>();
        _gained = new TreeSet<String>();
        _lost = new TreeSet<String>();
        _present_count = COUNT_DEFAULT;
        _gained_count = COUNT_DEFAULT;
        _lost_count = COUNT_DEFAULT;
    }

    public BinaryCharacters( final SortedSet<String> present_characters,
                             final SortedSet<String> gained_characters,
                             final SortedSet<String> lost_characters,
                             final String type ) {
        _present = present_characters;
        _gained = gained_characters;
        _lost = lost_characters;
        _type = type;
        _present_count = COUNT_DEFAULT;
        _gained_count = COUNT_DEFAULT;
        _lost_count = COUNT_DEFAULT;
    }

    public BinaryCharacters( final SortedSet<String> present_characters,
                             final SortedSet<String> gained_characters,
                             final SortedSet<String> lost_characters,
                             final String type,
                             final int present_count,
                             final int gained_count,
                             final int lost_count ) {
        _present = present_characters;
        _gained = gained_characters;
        _lost = lost_characters;
        _type = type;
        _present_count = present_count;
        _gained_count = gained_count;
        _lost_count = lost_count;
        validate();
    }

    public void addGainedCharacter( final String binary_character ) {
        if ( getLostCharacters().contains( binary_character ) ) {
            throw new IllegalArgumentException( "attempt to add binary character [" + binary_character
                    + "] to gained characters but is already listed as lost" );
        }
        getGainedCharacters().add( binary_character );
    }

    public void addLostCharacter( final String binary_character ) {
        if ( getPresentCharacters().contains( binary_character ) ) {
            throw new IllegalArgumentException( "attempt to add binary character [" + binary_character
                    + "] to lost characters but is already listed as present" );
        }
        if ( getGainedCharacters().contains( binary_character ) ) {
            throw new IllegalArgumentException( "attempt to add binary character [" + binary_character
                    + "] to lost characters but is already listed as gained" );
        }
        getLostCharacters().add( binary_character );
    }

    public void addPresentCharacter( final String binary_character ) {
        if ( getLostCharacters().contains( binary_character ) ) {
            throw new IllegalArgumentException( "attempt to add binary character [" + binary_character
                    + "] to present characters but is already listed as lost" );
        }
        getPresentCharacters().add( binary_character );
    }

    @Override
    public StringBuffer asSimpleText() {
        return asText();
    }

    @Override
    public StringBuffer asText() {
        validate();
        final StringBuffer sb = new StringBuffer();
        sb.append( "present [" );
        sb.append( getPresentCount() );
        sb.append( "]: " );
        sb.append( getPresentCharactersAsStringBuffer() );
        sb.append( ForesterUtil.LINE_SEPARATOR );
        sb.append( "gained  [ " );
        sb.append( getGainedCount() );
        sb.append( "]: " );
        sb.append( getGainedCharactersAsStringBuffer() );
        sb.append( ForesterUtil.LINE_SEPARATOR );
        sb.append( "lost    [" );
        sb.append( getLostCount() );
        sb.append( "]: " );
        sb.append( getLostCharactersAsStringBuffer() );
        return sb;
    }

    @Override
    /**
     * Not a deep copy.
     * 
     */
    public PhylogenyData copy() {
        validate();
        return new BinaryCharacters( getPresentCharacters(),
                                     getGainedCharacters(),
                                     getLostCharacters(),
                                     getType(),
                                     getPresentCount(),
                                     getGainedCount(),
                                     getLostCount() );
    }

    public SortedSet<String> getGainedCharacters() {
        return _gained;
    }

    public String[] getGainedCharactersAsStringArray() {
        return sortedSetToStringArray( getGainedCharacters() );
    }

    public StringBuffer getGainedCharactersAsStringBuffer() {
        return sortedSetToStringBuffer( getGainedCharacters(), " " );
    }

    public int getGainedCount() {
        return _gained_count;
    }

    public SortedSet<String> getLostCharacters() {
        return _lost;
    }

    public String[] getLostCharactersAsStringArray() {
        return sortedSetToStringArray( getLostCharacters() );
    }

    public StringBuffer getLostCharactersAsStringBuffer() {
        return sortedSetToStringBuffer( getLostCharacters(), " " );
    }

    public int getLostCount() {
        return _lost_count;
    }

    public SortedSet<String> getPresentCharacters() {
        return _present;
    }

    public String[] getPresentCharactersAsStringArray() {
        return sortedSetToStringArray( getPresentCharacters() );
    }

    public StringBuffer getPresentCharactersAsStringBuffer() {
        return sortedSetToStringBuffer( getPresentCharacters(), " " );
    }

    public int getPresentCount() {
        return _present_count;
    }

    public String getType() {
        return _type;
    }

    @Override
    public boolean isEqual( final PhylogenyData data ) {
        throw new UnsupportedOperationException();
    }

    public void setType( final String type ) {
        _type = type;
    }

    @Override
    public StringBuffer toNHX() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return asText().toString();
    }

    private void validate() {
        if ( ( getPresentCount() != COUNT_DEFAULT ) && ( getPresentCharacters().size() > 0 )
                && ( getPresentCount() != getPresentCharacters().size() ) ) {
            throw new RuntimeException( "present characters size and count are unequal" );
        }
        if ( ( getGainedCount() != COUNT_DEFAULT ) && ( getGainedCharacters().size() > 0 )
                && ( getGainedCount() != getGainedCharacters().size() ) ) {
            throw new RuntimeException( "gained characters size and count are unequal" );
        }
        if ( ( getLostCount() != COUNT_DEFAULT ) && ( getLostCharacters().size() > 0 )
                && ( getLostCount() != getLostCharacters().size() ) ) {
            throw new RuntimeException( "lost characters size and count are unequal" );
        }
    }

    private static String[] sortedSetToStringArray( final SortedSet<String> set ) {
        final String[] chars = new String[ set.size() ];
        final Iterator<String> it = set.iterator();
        int i = 0;
        while ( it.hasNext() ) {
            chars[ i++ ] = it.next();
        }
        return chars;
    }

    private static StringBuffer sortedSetToStringBuffer( final SortedSet<String> set, final String separator ) {
        final StringBuffer sb = new StringBuffer();
        final Iterator<String> it = set.iterator();
        while ( it.hasNext() ) {
            sb.append( it.next() );
            if ( it.hasNext() ) {
                sb.append( separator );
            }
        }
        return sb;
    }
}
