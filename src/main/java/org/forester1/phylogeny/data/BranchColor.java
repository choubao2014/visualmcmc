// $Id:
// FORESTER -- software libraries and applications
// for evolutionary biology research and applications.
//
// Copyright (C) 2008-2009 Christian M. Zmasek
// Copyright (C) 2008-2009 Burnham Institute for Medical Research
// All rights reserved
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
//
// Contact: phylosoft @ gmail . com
// WWW: https://sites.google.com/site/cmzmasek/home/software/forester

package org.forester1.phylogeny.data;

import java.awt.Color;

public class BranchColor implements PhylogenyData {

    private Color _color;

    public BranchColor() {
        _color = null;
    }

    public BranchColor( final Color color ) {
        _color = color;
    }

    @Override
    public StringBuffer asSimpleText() {
        return new StringBuffer( getValue().toString() );
    }

    @Override
    public StringBuffer asText() {
        return new StringBuffer( getValue().toString() );
    }

    @Override
    /**
     * Not a deep copy.
     * 
     */
    public PhylogenyData copy() {
        final BranchColor bc = new BranchColor();
        bc.setValue( getValue() );
        return bc;
    }

    public Color getValue() {
        return _color;
    }

    @Override
    public boolean isEqual( final PhylogenyData data ) {
        return getValue().equals( ( ( BranchColor ) data ).getValue() );
    }

    public void setValue( final Color color ) {
        _color = color;
    }

    @Override
    public StringBuffer toNHX() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return asText().toString();
    }
}
