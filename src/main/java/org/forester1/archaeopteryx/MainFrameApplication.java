package org.forester1.archaeopteryx;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import org.forester1.phylogeny.Phylogeny;
import org.forester1.util.ForesterUtil;

public final class MainFrameApplication extends MainFrame {

    private static int                 		 FRAME_X_SIZE                          = 1070;
    private static int                 		 FRAME_Y_SIZE                          = 550;
    private static final long                serialVersionUID                      = -799735726778865234L;

    public MainFrameApplication( final Phylogeny[] phys, final Configuration config, final int myX, final int myY, final String name ) {
        _configuration = config;
        if ( _configuration == null ) {
            throw new IllegalArgumentException( "configuration is null" );
        }
        FRAME_X_SIZE = myX;
        FRAME_Y_SIZE = myY;
        setVisible( false );
        setOptions( Options.createInstance( _configuration ) );
        _mainpanel = new MainPanel( _configuration);
        _contentpane = getContentPane();
        _contentpane.setLayout( new BorderLayout() );
        _contentpane.add( _mainpanel, BorderLayout.CENTER );
        setSize( MainFrameApplication.FRAME_X_SIZE, MainFrameApplication.FRAME_Y_SIZE );
        setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
        addWindowListener( new WindowAdapter() {

            @Override
            public void windowClosing( final WindowEvent e ) {
                exit();
            }
        } );
        setVisible( true );
        if ( ( phys != null ) && ( phys.length > 0 ) ) {
            AptxUtil.addPhylogeniesToTabs( phys, name, null, _configuration, _mainpanel, myX, 20);
            validate();
        }
/*		BufferedImage im = new BufferedImage(_contentpane.getWidth(), _contentpane.getHeight(), BufferedImage.TYPE_INT_ARGB);
		_contentpane.paint(im.getGraphics());
		try {
			ImageIO.write(im, "PNG", new File("/Users/rhali/Desktop/shot.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
        _contentpane.repaint();
    }

    public void end() {
        _contentpane.removeAll();
        setVisible( false );
        dispose();
    }

    @Override
    public MainPanel getMainPanel() {
        return _mainpanel;
    }

    void exit() {
        _contentpane.removeAll();
        setVisible( false );
        dispose();
    }
    
	public void changeSize(int Xsize, int Ysize) {
    	FRAME_X_SIZE = Xsize;
    	FRAME_Y_SIZE = Ysize;
    	this.setSize(new Dimension(Xsize, Ysize));
    }

    static void warnIfNotPhyloXmlValidation( final Configuration c ) {
        if ( !c.isValidatePhyloXmlAgainstSchema() ) {
            JOptionPane
                    .showMessageDialog( null,
                                        ForesterUtil
                                                .wordWrap( "phyloXML XSD-based validation is turned off [enable with line 'validate_against_phyloxml_xsd_schem: true' in configuration file]",
                                                           80 ),
                                        "Warning",
                                        JOptionPane.WARNING_MESSAGE );
        }
    }
} // MainFrameApplication.
