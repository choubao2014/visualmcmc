package se.cbb.vmcmc.examples;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import se.cbb.vmcmc.gui.MCMCWindow;
import se.cbb.vmcmc.libs.SequenceFileWriter;
import se.cbb.vmcmc.main.MCMCFileAnalyser;

public class MCMCExamplesFunctions {
	public static void readExampleFiles(MCMCWindow window, String fileName, boolean isParallelAnalysis) {
		InputStream is = window.getClass().getResourceAsStream(fileName);
    	if(is.equals(null)) {
    		System.out.println("File not found");
    		System.exit(-1);
    	}
    	try {
    		File f1 = new File(fileName.substring(1));
    		OutputStream out=new FileOutputStream(f1);
    		byte buf[]=new byte[1024];
    		int len;
    		while((len=is.read(buf))>0)
    			out.write(buf,0,len);
    		out.close();
    		is.close();
    		
    		File file = new File(fileName.substring(1));
    		MCMCFileAnalyser.fileOpener(file, window, false, isParallelAnalysis);
    		SequenceFileWriter.FileDeleter(fileName.substring(1));
    	} catch (IOException e){
    		System.err.println("Warning: File not found. Continuing without file.");
    	}
	}
	
	public static void readExampleParallelFiles(MCMCWindow window, String fileName) {
		InputStream is = window.getClass().getResourceAsStream(fileName);
    	if(is.equals(null)) {
    		System.out.println("File not found");
    		System.exit(-1);
    	}
    	try {
    		File f1 = new File(fileName.substring(1));
    		OutputStream out=new FileOutputStream(f1);
    		byte buf[]=new byte[1024];
    		int len;
    		while((len=is.read(buf))>0)
    			out.write(buf,0,len);
    		out.close();
    		is.close();
    		
    		File file = new File(fileName.substring(1));
    		MCMCFileAnalyser.fileOpener(file, window, true, true);
    		MCMCFileAnalyser.parallelFileOpener(file, window);
    		SequenceFileWriter.FileDeleter(fileName.substring(1));
    	} catch (IOException e){
    		System.err.println("Warning: File not found. Continuing without file.");
    	}
	}
	
	public static void readExampleBayesFiles(MCMCWindow window, String fileName) {
		InputStream is = window.getClass().getResourceAsStream(fileName);
    	if(is.equals(null)) {
    		System.out.println("File not found");
    		System.exit(-1);
    	}
    	try {
    		File f1 = new File(fileName.substring(1));
    		OutputStream out=new FileOutputStream(f1);
    		byte buf[]=new byte[1024];
    		int len;
    		while((len=is.read(buf))>0)
    			out.write(buf,0,len);
    		out.close();
    		is.close();
    	} catch (IOException e){
    		System.err.println("Warning: File not found. Continuing without file.");
    	}
	}
}
