package se.cbb.vmcmc.libs;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Handles all regular application parameters for VMCMC. Used by MCMCMainApplication class for parameter parsing in the main function.
 * 
 * @author Raja Hashim Ali.
 */
public class Parameters {

	/** Required parameters: None */
	@Parameter(description = "None or filename. VMCMC start window will appear, if no arguments are passed and VMCMC will display results and data in GUI if arguments filname is given.")
	public List<String> files = new ArrayList<String>();
	
	/** Help. */
	@Parameter(names = {"-h", "-help", "--help"}, description = "Display help. To understand the options and input parameters, use help.")
	public Boolean help = false;
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-n","--nogui"}, description = "Test and simple statistics shown on command line only. VMCMC computed statistics and tests shown on stdout.")
	public Boolean nogui = false;
	
	/** Test and Simple Statistics only */
	@Parameter(names = {"-c","--confidencelevel"}, description = "Confidence Level value.")
	public String confidence = "95";
	
	/** Test Statistics only */
	@Parameter(names = {"-t","--testresult"}, description = "Test statistics shown on command line only. VMCMC computed tests shown on stdout. ")
	public Boolean test = false;
	
	/** Simple Statistics only */
	@Parameter(names = {"-s","--simplestats"}, description = "Statistics shown on command line only. VMCMC computed statistics for the MCMC chain shown on stdout. ")
	public Boolean stats = false;
	
	/** Geweke Convergence Test only */
	@Parameter(names = {"-g","--geweke"}, description = "Geweke convergence test and burn in estimator result on command line only. VMCMC computed geweke convergence and burn in estimator for the MCMC chain shown on stdout. ")
	public Boolean geweke = false;
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-e","--ess"}, description = "Effective Sample Size test result on command line only. VMCMC computed estimated sample size burn-in and convergenceestimator for the MCMC chain shown on stdout. ")
	public Boolean ess = false;
	
	/** Gelman-Rubin convergence test only */
	@Parameter(names = {"-r","--gelmanrubin"}, description = "Gelman-Rubin convergence test result on command line only. VMCMC computed gelman rubin burn-in and convergence estimator for the MCMC chain shown on stdout. ")
	public Boolean gr = false;
	
	/** Display Posterior distribution of trees only */
	@Parameter(names = {"-p","--posterior"}, description = "Display Posterior distribution of trees. ")
	public Boolean posterior = false;
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-b","--burnin"}, description = "When loading single trace, use this burnin for trace. Default ('-1') means 'use burnin estimate from the Max-ESS estimator'!")
	public String burnin = "-1";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-b1","--burnin1"}, description = "When loading parallel traces, use this burnin for trace 1. Default ('-1') means 'use burnin estimate from the Max-ESS estimator'!")
	public String burnin1 = "-1";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-b2","--burnin2"}, description = "When loading parallel traces, use this burnin for trace 2. Default ('-1') means 'use burnin estimate from the Max-ESS estimator'!")
	public String burnin2 = "-1";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-m","--maxaposterioritree"}, description = "Calculate and display MAP tree. ")
	public Boolean maptree = false;
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-ct","--convergencetest"}, description = "Run the overall convergence tests only. ")
	public Boolean convergencetest = false;
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-sd","--sampledata"}, description = "Sample a data point uniformly from converged MCMC chain. ")
	public Boolean sampledata = false;
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-o","--outfile"}, description = "Output command line options on standard output or in a specified file. ")
	public String outFile = "stdout";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-pa","--path"}, description = "Path where to make the output file for command line VMCMC. ")
	public String path = "./";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-a","--alpha"}, description = "Alpha value for parallel chain analysis. ")
	public String alpha = "0.05";
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-tt","--truetreeposterior"}, description = "Verify if the true tree provided in file1 is present in the posterior of MCMC file provided in file2. ")
	public Boolean trueTest = false;
	
	/** Estimated Sample Size Test only */
	@Parameter(names = {"-cbp","--convertmrbayestojprime"}, description = "Convert a MrBayes MCMC file to a JPrIMe one. ")
	public Boolean convertMrBayes2jprime = false;
	
	/** File containing parameter values */
	@Parameter(names = {"-pf","--parameterfile"}, description = "File containing parameters for VMCMC. ")
	public String paramfile = "";
	
	/** Tree parameter convergence only **/
	@Parameter(names = {"-tc","--treeconvergence"}, description = "Check for convergence of split distributions using a Chi Square test. For a single trace file. The test removes burnin, partitions the remaining trace into two samples, and performs a Chi Square test to assess convergence of a tree parameter.")
	public boolean treeconvergence= false;
}
