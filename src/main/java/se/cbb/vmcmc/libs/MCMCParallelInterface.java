package se.cbb.vmcmc.libs;

public interface MCMCParallelInterface {
	void 				setDataContainer			(MCMCDataContainer datacontainer, int index);
	void 				setSeriesID					(int id);
	void 				setBurnIn					(double burnin);
	int 				getSeriesID					();
	MCMCDataContainer 	getDataContainer			(int index);
}
