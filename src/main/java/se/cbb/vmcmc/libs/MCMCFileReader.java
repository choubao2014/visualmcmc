package se.cbb.vmcmc.libs;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import se.cbb.vmcmc.newickhandler.Tree;

/**
 * MCMCFileReader: Static class responsible for handling files. Interprets files of formats
 * (.mcmc) and stores the results as a MCMCDataContainer. Class containins static methods for 
 * converting byte arrays to appropriate datastructures.
 */
abstract public class MCMCFileReader {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	static int p;	//Pointer to current position in file.
	public static String program;
	static int count1;

	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	/** formatCommentLine: Extracts names and types into two strings separated by spaces.*/
	private static String[] formatCommentLine(char[] data) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		String 			names;
		String 			types;

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		names 			= "";
		types 			= "";
		
		/* ******************** FUNCTION BODY ************************************* */
		for(int i=0; i<data.length && data[i] != '\r' && data[i] != '\n'; i++) {
			if(data[i] == '#') {		//Skip comment char
				if(data[i+1] == ' ')	//If followed by space; skip space.
					i++;
				continue;
			}
			if(data[i] == '\t')		//Replace tabs with spaces for conformity
				data[i] = ' ';
			if(data[i] == ';')		//Skip ; chars
				continue;

			if(data[i] != ' ') {	//Space means new parameter
				//Extract name:
				while(data[i] != '(' && data[i] != ' ') {
					names += data[i];
					i++;
				}
				names += ' ';	//Add space between names

				if(data[i] == ' ')	//If there is no type add "none" as type
					types += "none ";
				else {			//Otherwise extract type
					i++;
					while(data[i] != ')') {
						types += data[i];
						i++;
					}
					types += ' ';	//Add space between types
				}
			}
		}
		String[] returnValues = {names, types};
		return returnValues;
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** formatCommentLine: Extracts names and types into two strings separated by spaces.*/
	private static String[] formatCommentLine1(char[] data, char[] firstLine) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		String 			names;
		String 			types;
		String 			dataEntry;

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		names 			= "";
		types 			= "";
		
		/* ******************** FUNCTION BODY ************************************* */
		for(int i=0; i<data.length && data[i] != '\r' && data[i] != '\n'; i++) {
			if(data[i] == '\t')		//Replace tabs with spaces for conformity
				data[i] 		= ' ';
			if(data[i] == ';')		//Skip ; chars
				continue;
			if(data[i] != ' ') {	//Space means new parameter
				//Extract name:
				while(data[i] != ' ' && data[i] != '\t' && data[i] != '\r' && i < data.length-1) {
					names += data[i];
					i++;
				}
				names += ' ';	//Add space between names
			}
		}
		
		for(int i=0; i<firstLine.length && firstLine[i] != '\r' && firstLine[i] != '\n'; i++) {
			dataEntry 				= "";
			if(firstLine[i] == '\t')		//Replace tabs with spaces for conformity
				firstLine[i] 		= ' ';
			if(firstLine[i] == ';')		//Skip ; chars
				continue;
			if(firstLine[i] != ' ') {	//Space means new parameter
				//Extract data entry:
				while(firstLine[i] != ' ' && firstLine[i] != '\t' && firstLine[i] != '\r' && i < firstLine.length-1) {
					dataEntry += firstLine[i];
					i++;
				}
				try{
					Double.parseDouble(dataEntry);
					types += "float ";
				} catch(Exception e) {
					types += "tree ";
				}
			}
		}
		String[] returnValues = {names, types};
		return returnValues;
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/**
	 * readNumber: Converts a byte array terminated by \t or ' ' to Double.
	 */
	private static Double readNumber(byte[] data, boolean gui) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		String 				number;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		number 				= "";
		
		/* ******************** FUNCTION BODY ************************************* */
		while(p < data.length && data[p] != '\t' && data[p] != ' ' && data[p] != '\r' && data[p] != '\n') {
			number += (char) data[p];
			p++;
			if(p < data.length && data[p] == ';')
				p++;
		}
		
		if(!number.equals("") && !number.equals("Infinity") && !number.equals("nfinity") && !number.equals("NA")) 
			return Double.parseDouble(number);
		else if(number.equals("Infinity") || number.equals("nfinity") || number.equals("NA")) {
			if(gui == false)
				return Double.POSITIVE_INFINITY;
			else {
				JOptionPane.showMessageDialog(new JFrame(), "MCMC Chain contains Infinity in one of its numeric paramaters. Invalid value for MCMC parameter.");
				return null;
			}
		} else
			return Double.parseDouble("1000000000");
		/* ******************** END OF FUNCTION *********************************** */
	}

	/**
	 * readLine: Extract sub array terminated by \n from byte array. 
	 */
	private static byte[] readLine(byte[] data, int sizeofFile) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		byte[] 				line;
		int 				size;
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		size 				= 0;
		
		/* ******************** FUNCTION BODY ************************************* */
		while(p < sizeofFile && data[p] != '\n') {
			p++;
			size++;
		}

		line = new byte[size];
		System.arraycopy(data, p-size, line, 0, size);
		return line;
		/* ******************** END OF FUNCTION *********************************** */
	}

	/**
	 * readTreeStructure: Recursive function that converts byte array to treestructure MCMCTreeNode.
	 */
	private static MCMCTreeNode readTreeStructure(byte[] data, final int level) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		MCMCTreeNode 		node;
		String 				name;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		node 				= new MCMCTreeNode();
		name 				= "";
		
		/* ******************** FUNCTION BODY ************************************* */
		node.setLevel(level);
		if(data[p] == '(') {											//Left parenthesis marks the existence of children to be added
			p++;														//Skip left parenthesis
			node.addChild(readTreeStructure(data, level+1));			//Add one child per default
			
			while(data[p] != ')') {										//Check for more children until right parenthesis is found
				if(data[p] == ',') {									//Comma means one more child to be added
					p++;												//Skip comma
					if(data[p] == ' ')
						p++;											//Skip space
					node.addChild(readTreeStructure(data, level+1));	//Add child
				}
			}	
			p++;														//Skip right parenthesis
		} else if(data[p]!= ':'){
			while(data[p] != ',' && data[p] != ')') {					//Extract name of node
				name += (char) data[p];
				p++;													//Skip character
			}
			node.setName(name);											//Set the nodes name
		}
		
		if(p < data.length && data[p] == ':') {							//If there is a colon attached after non-leaf node. Ignore it.
			if(program.equals("OTHER")){
				while(data[p] != ']')
					p++;
				p++;
			} else if (program.equals("JPrIMe")){
				while(p < data.length && data[p] != ',' && data[p] != ')') {
					p++;
				}
			} else {
				
			}
		}
		return node;													//Return created node
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/**
	 * findArrayKey: Creates and returns an unique key for specified byte array.
	 */
	private static int findArrayKey(byte[] data) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		int 		key;
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		key			= 0;	
		/* ******************** FUNCTION BODY ************************************* */
		for(int i = 0; i < data.length; i++)
			key += data[i]*i;
		return key;
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/**
	 * readTreeArray: Extract and return tree as newick byte array. This array can later
	 * be converted to a treeStructure through the method treeArrayToTreeNode.
	 */
	private static byte[] readTreeArray(byte[] data) throws Exception { 
		/* ******************** FUNCTION VARIABLES ******************************** */
		int 		size;
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		size		= 0;
		
		/* ******************** FUNCTION BODY ************************************* */
		while(data[p] != ';') {
			p++;
			size++;
		}
		byte[] tree = new byte[size];
		System.arraycopy(data, p-size, tree, 0, size);
		p++;
		return tree;
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/**
	 * readTreeArray: Extract and return tree as newick byte array. This array can later
	 * be converted to a treeStructure through the method treeArrayToTreeNode.
	 */
	private static byte[] readTreeArrayWithoutLengths(byte[] data) throws Exception { 
		/* ******************** FUNCTION VARIABLES ******************************** */
		int size = 0;
		int pos = p;
		int index = 0;

		while(data[pos] != ';') {
			pos++;
			size++;
			if(data[pos] == ':') {
				while(data[pos] != ';' & data[pos] != ',' & data[pos] != ')' ) {
					pos++;
				}
			}
		}
		size++;
		byte[] tree = new byte[size];
		
		while(p <= pos) {
			tree[index] = data[p];
			p++;
			index++;
			if(data[p] == ':') {
				while(data[p] != ';' & data[p] != ',' & data[p] != ')' ) 
					p++;
			}
		}
		return tree;
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */	
	/**
	 * treeArrayToTreeNode: same functionality as method readTreeStructure(byte[]) but with p=0.
	 */
	public static MCMCTreeNode treeArrayToTreeNode(byte[] data) {
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		p = 0;
		/* ******************** FUNCTION BODY ************************************* */
		return readTreeStructure(data, 0);
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	private static String mapNexusToSpecies(ArrayList<String> speciesName, String unmappedNewick) {
		String[] strArr = unmappedNewick.split(":");
		String retStr = "";
		for(int i = 0; i < strArr.length; i++) {
			int index = 0;
			for(int j = strArr[i].length() - 1; j > -1; j--) {
				if(strArr[i].toCharArray()[j] == ',' || strArr[i].toCharArray()[j] == '(') {
					index =  j + 1;
					break;
				}
			}
			if(index == 0) 
				retStr = retStr + strArr[i];
			else {
				String checkStr = strArr[i].substring(index);
				retStr = retStr + strArr[i].substring(0, index);
				retStr = retStr + speciesName.get(Integer.parseInt(checkStr) - 1);
			}
			retStr = retStr + ":";
		}
		return retStr;
	}
	
	@SuppressWarnings("unused")
	private static String mapNexusToSpeciesWithoutLength(ArrayList<String> speciesName, String unmappedNewick) {
		String[] strArr = unmappedNewick.split(":");
		String retStr = "";

		for(int i = 0; i < strArr.length; i++) {
			if(i == 0) {
				unmappedNewick = strArr[0];
			} else {
				String[] strArr1 = strArr[i].split(",");
				if(strArr1.length > 1) {
					unmappedNewick = unmappedNewick + "," + strArr1[1];
				} else {
					unmappedNewick = unmappedNewick + ")";
				}
			}			
		}		
		
		strArr = unmappedNewick.split(",");
		for(int i = 0; i < strArr.length; i++) {
			System.out.println(strArr[i]);
			if (i == 0) {
				int j = 0;
				while(strArr[i].charAt(j) != '(') {
					j++;
				}
				while(strArr[i].charAt(j) == '(') {
					j++;
				}
				String checkStr = strArr[i].substring(j);
				System.out.println(checkStr);
				retStr = retStr + strArr[i].substring(0, j);
				retStr = retStr + speciesName.get(Integer.parseInt(checkStr) - 1) + ",";
			} else if(i == strArr.length - 1) {
				int j = strArr[i].length()-1;
				while(strArr[i].charAt(j) == ')') 
					j--;
				String checkStr = strArr[i].substring(0, j+1);
				retStr = retStr + strArr[i].substring(j);
				retStr = retStr + speciesName.get(Integer.parseInt(checkStr) - 1);
			} else if(strArr[i].charAt(0) == '(') {
				int j = 0;
				while(strArr[i].charAt(j) == '(') {
					j++;
				}
				String checkStr = strArr[i].substring(j);
				System.out.println(checkStr);
				retStr = retStr + strArr[i].substring(0, j);
				retStr = retStr + speciesName.get(Integer.parseInt(checkStr) - 1) + ",";
			} else if(strArr[i].charAt(strArr[i].length()-1) == ')') {
				int j = strArr[i].length()-1;
				while(strArr[i].charAt(j) == ')') 
					j--;
				String checkStr = strArr[i].substring(0, j+1);
				retStr = retStr + strArr[i].substring(j);
				retStr = retStr + speciesName.get(Integer.parseInt(checkStr) - 1) + ",";
			} else {
				retStr = retStr + speciesName.get(Integer.parseInt(strArr[i]) - 1) + ",";
			}
		}
		
		return retStr;		
	}

	/**
	 * readMCMCFile: Reads file of format mcmc and extracts information to MCMCDataContainer. 
	 * @throws Exception 
	 */
	public static MCMCDataContainer readMCMCFile(File file, boolean gui, boolean conversion) throws Exception {
		/* ******************** FUNCTION VARIABLES ******************************** */
		MCMCDataContainer 		datacontainer;
		FileInputStream 		inputStream;
		int 					size;
		int 					lineIndex;
		int 					numTypes;
		int 					valueindex;
		int 					treeindex;
		int 					key;
		int						colIterInData;
		int						colIterInData2;
		byte[] 					commentline;
		String 					values[];
		String 					names;
		String					name;
		String 					types;
		String 					type;
		StringTokenizer 		nameTokenizer;
		StringTokenizer 		typeTokenizer;
		Double 					number;
		byte[] 					treeArray;
		byte[] 					data;
		MCMCTree 				tree;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		datacontainer 			= new MCMCDataContainer();
		commentline				= null;
		lineIndex	 			= 0;
		colIterInData			= 0;
		colIterInData2          = -1;
		program 				= "JPrIMe"; //MCMC chain has been generated from JPrIMe or CODA or BAli-Phy
		
		/* ******************** FUNCTION BODY ************************************* */
		size = (int) file.length();
		data = new byte[size];		
		datacontainer.setFileName(file.getName());
		
		inputStream = new FileInputStream(file);
		inputStream.read(data, 0, size);			//Read entire file to memory for fast processing
		
		p = 0;
		if(data[p] == '#') {	//MCMC chain has been generated from PrIMe
			while(data[p] == '#') {
				commentline = readLine(data, size);	//Store comment line
				p++;
			}
			if((data[p] >= '0' && data[p] <= '9') || data[p] == '-')
				program = "PrIMe"; 
			else
				program = "Beast";
		} else if(data[p] == '[') {
			program = "MrBayes";
			while(data[p] == '[') {
				commentline = readLine(data, size);	//Store comment line
				p++;
			}
		} else {
			program = "JPrIMe";
		}
		count1 = 0;
		
		for(; p<size; p++) {
			if(data[p] == '#') {	//MCMC chain has been generated from PrIMe	
				readLine(data, size);	//Store comment line
				continue;
			}
						
			//Reaching this clause means end top portion of comments. Process last comment line which stores parameter names and types
			numTypes = 0;
			if(datacontainer.getNumSeries() == 0) {
				values = null;
				if(program.equals("PrIMe")) {			//Extract names and types from stored comment line
					String firstLine = new String(commentline);
					if(firstLine.equals("#")) {
						commentline = readLine(data, size);
						p++;
						byte[] firstDataLine = readLine(data, size);
						p = p-firstDataLine.length;
						values = formatCommentLine1(new String(commentline).toCharArray(), new String(firstDataLine).toCharArray());
					} else
						values = formatCommentLine(firstLine.toCharArray());
				} else {
					commentline = readLine(data, size);
					p++;
					byte[] firstDataLine = readLine(data, size);
					p = p-firstDataLine.length;
					values = formatCommentLine1(new String(commentline).toCharArray(), new String(firstDataLine).toCharArray());
					values[0] = values[0].substring(0, values[0].length() - 1) + new String(commentline).charAt(commentline.length - 1);
				}
				names = values[0];
				types = values[1];

				nameTokenizer = new StringTokenizer(names);
				typeTokenizer = new StringTokenizer(types);
				
				//Store datacontainer with information about names and types
				while(nameTokenizer.hasMoreTokens() && typeTokenizer.hasMoreTokens()) {
					type = typeTokenizer.nextToken();
					name = nameTokenizer.nextToken();
					
					if(name.equalsIgnoreCase("iteration") || name.equalsIgnoreCase("n") || name.equalsIgnoreCase("gen") || name.equalsIgnoreCase("state") || name.equalsIgnoreCase("iter"))
						colIterInData = numTypes;
					if(name.equalsIgnoreCase("aamodel"))
						colIterInData2 = numTypes;
					numTypes++;

					//Tree parameters will be read as newick strings for later handling
					if(colIterInData2 != -1) {
						datacontainer.addValueName(name);
						datacontainer.addSerieType(SerieType.FLOAT);
						datacontainer.addNewValueSerie();
					} else if(type.compareTo("tree") == 0) {
						datacontainer.addTreeName(name);
						datacontainer.addSerieType(SerieType.TREE);
						datacontainer.addNewTreeSerie();
					} else if(type.compareTo("none") == 0 || type.compareTo("float") == 0 || type.compareTo("logfloat") == 0) {
						datacontainer.addValueName(name);
						datacontainer.addSerieType(SerieType.FLOAT);
						datacontainer.addNewValueSerie();
					} else {							//Other types are ignored
						datacontainer.addSerieType(SerieType.OTHER);
					}
				}
			}
			
			valueindex = 0;
			treeindex = 0;
			for(int i = 0; i < datacontainer.getNumSeries(); i++) {	//For each row. Iterate all parameters and read value according to type
				switch(datacontainer.getSerieTypes().get(i)) {
				case FLOAT:
					number = readNumber(data, gui);
					if(number!=(double)(1000000000) && number != Double.POSITIVE_INFINITY) {
						datacontainer.addValueToSerie(number, valueindex);
						valueindex++;
					} else if (number == Double.POSITIVE_INFINITY) {
						System.out.println("\t\"Error in MCMC input format\" : \"MCMC Chain contains Infinity in one of its numeric paramaters. Invalid value for MCMC parameter.\"\n}");
						System.exit(-1);
					} else {
						datacontainer.addValueToSerie(Double.parseDouble("1"), valueindex);
						valueindex++;
					}
					break;

				case TREE:
					treeArray = readTreeArrayWithoutLengths(data);
					key = findArrayKey(treeArray);
					tree = new MCMCTree();
					tree.setKey(key);
					tree.setData(treeArray);
					tree.addDuplicate();
					tree.addIndex(lineIndex);
					datacontainer.addTreeToSerie(tree, treeindex);
					treeindex++;
					break;

				default: 			//If type does not match any of the enumerations above. Skip column
					while(p < data.length && data[p] != '\t' && data[p] != ' ')
						p++;	//Skip character
					break;
				}
				p++;
			}
			lineIndex++;	//Index of current line handled
		}

		if(conversion == false)
			datacontainer.removeSerie(colIterInData);
		datacontainer.setNumLines(lineIndex);	//Lineindex will equal total amount of lines at end of reading
		inputStream.close();
		
		ArrayList<String> valuenames = datacontainer.getValueNames();
		for(int i = 0; i < valuenames.size(); i++) {
			if(valuenames.get(i).equals("aamodel")) {
				datacontainer.removeSerie(i);
				break;
			}
		}
		
		if(program.equals("MrBayes")) {
			if(file.getAbsolutePath().endsWith(".p"))
				file = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length()-2) + ".t");
			else
				file = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length()-5) + ".tree");
			
			if(!file.exists()) 
				return datacontainer;
			datacontainer.addTreeName("GuestTree");
			datacontainer.addSerieType(SerieType.TREE);
			datacontainer.addNewTreeSerie();

			size 					= (int) file.length();
			commentline				= null;
			lineIndex	 			= 0;
			colIterInData			= 0;
			data 					= new byte[size];

			inputStream = new FileInputStream(file);
			inputStream.read(data, 0, size);			//Read entire file to memory for fast processing

			treeindex = 0;
			lineIndex = 0;
			
			byte[] mapLine;
			boolean check = false;
			ArrayList<String> speciesName = new ArrayList<String>(); 
						
			for(p = 0; p < size; p++) {
				if(check == false) {
					check = true;
					while(data[p] == '#' || data[p] == '[') {
						readLine(data, size);
						p++;
					}
					readLine(data, size);
					p++;
					readLine(data, size);

					while(true) {
						p++;
						if(data[p] == ' ' && data[p+1] == ' ' && data[p+2] == ' ' && data[p+3] == 't' && data[p+4] == 'r')
							break;
						else {
							mapLine = readLine(data, size);
							String tempMap = new String(mapLine);
							String[] strArr = tempMap.split(" ");
							speciesName.add(strArr[strArr.length-1].substring(0, strArr[strArr.length-1].length() - 1));
						}
					}
				}
				
				if(data[p] == 'e' && data[p+1] == 'n' && data[p+2] == 'd' && data[p+3] == ';')
					break;
				while(data[p] != '(')
					p++;
				treeArray = readTreeArray(data);
				
				Tree tree1 = new Tree(mapNexusToSpecies(speciesName, new String(treeArray)));
				se.cbb.vmcmc.newickhandler.Node root1 = tree1.getTree();
				root1.sortChildrenAlphabetically();
				se.cbb.vmcmc.newickhandler.Node temp = root1.rerootTree(root1);
				
				String nexusTree = temp.toStringWithoutLength();
				byte[] byteNexus = nexusTree.getBytes();
				
				key = findArrayKey(byteNexus);
				tree = new MCMCTree();
				tree.setKey(key);
				tree.setData(byteNexus);
				tree.addDuplicate();
				tree.addIndex(lineIndex);
				datacontainer.addTreeToSerie(tree, treeindex);
				lineIndex++;
			}
		}
		
		if(program.equals("Beast")) {
			if(file.getAbsolutePath().endsWith(".log") || file.getAbsolutePath().endsWith(".txt"))
				file = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length()-4) + ".trees");
			
			if(!file.exists()) 
				file = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length()-6) + ".t");
			if(!file.exists())
				return datacontainer;
			datacontainer.addTreeName("GuestTree");
			datacontainer.addSerieType(SerieType.TREE);
			datacontainer.addNewTreeSerie();

			size 					= (int) file.length();
			commentline				= null;
			lineIndex	 			= 0;
			colIterInData			= 0;
			data 					= new byte[size];

			inputStream = new FileInputStream(file);
			inputStream.read(data, 0, size);			//Read entire file to memory for fast processing

			treeindex = 0;
			lineIndex = 0;
			
			byte[] mapLine;
			boolean check = false;
			ArrayList<String> speciesName = new ArrayList<String>(); 
						
			for(p = 0; p < size; p++) {
				if(check == false) {
					check = true;
					while(data[p] == '#' || data[p] == '[' || data[p] == '\n' || data[p] == ' ') {
						readLine(data, size);
						p++;
					}
					commentline = readLine(data, size);
					String line = new String(commentline);
					while(!line.endsWith("Translate")) {
						commentline = readLine(data, size);
						p++;
						line = new String(commentline);
					}
					mapLine = readLine(data, size);
					String tempMap = new String(mapLine);
					while(!tempMap.endsWith(";")) {
						String[] strArr = tempMap.split(" ");
						speciesName.add(strArr[strArr.length-1].substring(0, strArr[strArr.length-1].length() - 1));
						p++;
						mapLine = readLine(data, size);
						tempMap = new String(mapLine);
					}
				}
				
				if(data[p] == 'E' && data[p+1] == 'n' && data[p+2] == 'd' && data[p+3] == ';')
					break;
				while(data[p] != '(')
					p++;
				treeArray = readTreeArray(data);
				
				Tree tree1 = new Tree(mapNexusToSpecies(speciesName, new String(treeArray)));
				se.cbb.vmcmc.newickhandler.Node root1 = tree1.getTree();
				root1.sortChildrenAlphabetically();
				se.cbb.vmcmc.newickhandler.Node temp = root1.rerootTree(root1);
				
				String nexusTree = temp.toStringWithoutLength();
				byte[] byteNexus = nexusTree.getBytes();
				
				key = findArrayKey(byteNexus);
				tree = new MCMCTree();
				tree.setKey(key);
				tree.setData(byteNexus);
				tree.addDuplicate();
				tree.addIndex(lineIndex);
				datacontainer.addTreeToSerie(tree, treeindex);
				lineIndex++;
			}
		}
		
		return datacontainer;
		/* ******************** END OF FUNCTION *********************************** */
	}
}
