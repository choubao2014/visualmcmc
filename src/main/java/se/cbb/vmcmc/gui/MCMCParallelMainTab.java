package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCMath;

public class MCMCParallelMainTab extends MCMCStandardTab {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private static final long 	serialVersionUID = 1L;
	private MCMCGraphToolParallelPanel 	graphtoolPanel;
	private MCMCDisplayPanel 	filePanel;
	private MCMCDisplayPanel 	mathPanel;
	private MCMCDisplayPanel 	burninPanel;
	private MCMCDisplayPanel 	burninPanel2;
	private MCMCDisplayPanel 	wilcoxonRankPanel;
	@SuppressWarnings("rawtypes")
	private JComboBox 			droplist;
	@SuppressWarnings("rawtypes")
	private JComboBox 			droplist1;
	private JTextField 			burninField;
	private Thread[] 			workerThreads;
	private Lock 				displaypanelLock;
	private double 				confidencelevel;
	private double 				alpha;
	private MCMCDataContainer 	container1;
	private MCMCDataContainer 	container2;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MCMCParallelMainTab(MCMCWindow parent) {
		super();

		this.container1 = parent.getDataContainer(1);
		this.container2 = parent.getDataContainer(2);
		displaypanelLock = new ReentrantLock();
		confidencelevel = 0.95;
		alpha = 0.05;

		graphtoolPanel = new MCMCGraphToolParallelPanel();
		workerThreads = new Thread[9];

		for(int i=0; i<workerThreads.length; i++)
			workerThreads[i] = new Thread();

		westpanel.setLayout(new FlowLayout());
		westpanel.setMinimumSize(new Dimension(300, 0));
		westpanel.setPreferredSize(new Dimension(300, 0));
		westpanel.setBackground(new Color(0xFFEEEEFF));

		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.X_AXIS));

		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(graphtoolPanel);
		westpanel.add(createDisplayPanels());
		
		southPanel.add(Box.createHorizontalGlue());
		southPanel.add(Box.createRigidArea(new Dimension(10, 0)));
	}
	
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */

	/** CreateDisplayPanels: Creates and adds panels responsible showing, file information, statistics and burn in. */
	@SuppressWarnings("rawtypes")
	private JPanel createDisplayPanels() {
		JPanel panel = new JPanel();
		droplist = new JComboBox();
		droplist.setBackground(Color.WHITE);
		droplist1 = new JComboBox();
		droplist1.setBackground(Color.WHITE);

		final JTextField confidencelevelField = new JTextField();	//Text field for bayesian confidence level
		burninField = new JTextField();	//Text field for burn in

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(new Color(0xFFEEEEFF));

		String[] paramnames = {"Parameter Selected:"};
		
		String[] burninNames = {"Burn in:", "Enter burnin", "Est. burnin (ESS): ", "Est. burnin (Geweke): ", "Gelman-Rubin Est. : "};
		JComponent[] burninComponents = {new JLabel(), burninField, new JLabel(), new JLabel(), new JLabel()};	

		String[] burninNames2 = {"Burn in:", "Est. burnin (ESS): ", "Est. burnin (Geweke): ", "Gelman-Rubin Est. : "};
		JComponent[] burninComponents2 = {new JLabel(), new JLabel(), new JLabel(), new JLabel()};
		
		String[] wilcoxonrank = {"Size of Sample1: ", "Size of Sample2: ", "Test statistic: ", "p-Value:", "Decision:"};
		JComponent[] wilcoxoncomponents = {new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};

		confidencelevelField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					confidencelevel = Double.parseDouble(confidencelevelField.getText())/100;
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid number. Only numbers 0-100 allowed.");
				}

				if(confidencelevel > 1.0)
					confidencelevel = 1.0;
				else if(confidencelevel < 0)
					confidencelevel = 0;

				updateDisplayPanels();
			}
		});

		filePanel = new MCMCDisplayPanel(paramnames, "Parameter");
		mathPanel = new MCMCDisplayPanel("Combined Chain Parameter Statistics");

		mathPanel.addComponent("Arithmetic mean: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Arithmetic Std. Dev.: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Geometric mean: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Geometric Std Dev.: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Harmonic mean: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Maximum value: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Minimum value: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Confidence level: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Bayesian confidence: ", new JLabel(), new Dimension(275, 18));

		burninPanel = new MCMCDisplayPanel("Burnin and Convergence Chain1");
		burninPanel2 = new MCMCDisplayPanel("Burnin and Convergence Chain2");
		wilcoxonRankPanel = new MCMCDisplayPanel("Mann Whitney U test");

		mathPanel.add(confidencelevelField);
		mathPanel.addComponent("Enter confidence level:", confidencelevelField, new Dimension(275, 18));

		filePanel.add(Box.createRigidArea(new Dimension(0, 5)));
		filePanel.add(droplist);
		filePanel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		filePanel.setToolTipText("Parameter Panel");
		mathPanel.setToolTipText("Parameter Statistics Panel");
		burninPanel.setToolTipText("Burnin and Convergence Panel for Trace1");
		burninPanel2.setToolTipText("Burnin and Convergence Panel for Trace2");
		wilcoxonRankPanel.setToolTipText("Nonparametric two-sample Wilcoxon Rank Sum test for checking if both parameters have been sampled from the same distribution.");

		burninPanel.addComponents(burninNames, burninComponents, new Dimension(275, 18));
		burninPanel2.addComponents(burninNames2, burninComponents2, new Dimension(275, 18));
		wilcoxonRankPanel.add(droplist1);
		wilcoxonRankPanel.addComponents(wilcoxonrank, wilcoxoncomponents, new Dimension(275, 18));
		
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(filePanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(mathPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(wilcoxonRankPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(burninPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(burninPanel2);
		
		return panel;
	}
		
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	/** updateDisplayPanels: Will calculate, format and display information for each display panel in the left panel. */
	public void updateDisplayPanels(){
		final NumberFormat formatter = new DecimalFormat("0.#####E0");
		final NumberFormat formatter1 = new DecimalFormat("0.###E0");

		if(datacontainer != null) {
			final Object[] serie 		= datacontainer.getValueSerie(seriesID).toArray();
			final Object[] serie1 		= container1.getValueSerie(seriesID).toArray();
			final Object[] serie2		= container2.getValueSerie(seriesID).toArray();
			String[] paramName 			= {datacontainer.getFileName()};

			filePanel.update(paramName);
			mathPanel.labels.get(7).setText(confidencelevel*100 + "%");

			int numPoints 				= serie.length;	
			final int numBurnInPoints 	= (int)(numPoints*burnin);	
			int numPoints1 			= serie1.length;	
			int numBurnInPoints1 	= (int)(numPoints1*burnin);	
			int numPoints2 				= serie2.length;	
			int numBurnInPoints2 		= (int)(numPoints2*burnin);		
			int ess1;
			int geweke1;
			boolean gelmanRubin1;
			
			if(numPoints1-numBurnInPoints1 > 0 && numPoints2-numBurnInPoints2 > 0) {
				int ess;
				int geweke;
				boolean gelmanRubin;

				if(numPoints < 100) {
					geweke 					= -2;
					ess 					= -1;
					gelmanRubin				= false;
				} else {
					ess 					= MCMCMath.calculateESSmax(serie);
					geweke 					= MCMCMath.calculateGeweke(serie);
					gelmanRubin 			= MCMCMath.GelmanRubinTest(serie, numBurnInPoints);
				}

				for(int t=0; t<workerThreads.length; t++)
					workerThreads[t].interrupt();

				final Double[] data 	= new Double[serie.length-numBurnInPoints];
				System.arraycopy(serie, numBurnInPoints, data, 0, serie.length-numBurnInPoints);

				//Thread updating arithmetic mean.
				workerThreads[0] = new Thread() {
					public void run() {
						double values 	= 0;

						for(int i = 0; i < data.length; i++)
							values+= (Double)data[i];

						double result 	= values/data.length;

						displaypanelLock.lock();
						mathPanel.labels.get(0).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[0].start();

				//Thread updating arithmetic standard deviation.
				workerThreads[1] = new Thread() {
					public void run() {

						double sigmaSquare = 0;
						double mean, values = 0;
						int i;
						for(i = 0; i < data.length; i++)
							values+= (Double)data[i];

						i = 0;
						mean = values/data.length;
						for(; i < data.length; i++)
							sigmaSquare+= java.lang.Math.pow((Double)data[i] - mean,2);

						double result = (double)java.lang.Math.sqrt(sigmaSquare/(i-1));

						displaypanelLock.lock();
						mathPanel.labels.get(1).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[1].start();

				//Thread updating geometric mean
				workerThreads[2] = new Thread() {
					public void run() {
						double values 	= 0;
						
						for(int i = 0; i < data.length; i++) 
							values += java.lang.Math.log((Double)data[i]);
						
						double result = java.lang.Math.exp(values/(double)data.length);
			
						displaypanelLock.lock();
						if(Double.isNaN(result))
							mathPanel.labels.get(2).setText("-");
						else
							mathPanel.labels.get(2).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[2].start();

				//Thread updating geometric standard deviation
				workerThreads[3] = new Thread() {
					public void run() {
				        double sum = 0;
				        double values 	= 0;
						
						for(int i = 0; i < data.length; i++) 
							values += java.lang.Math.log((Double)data[i]);
						
						values = java.lang.Math.exp(values/(double)data.length);
						
				        int numValues = data.length;
				        
				        for(int i = 0; i < numValues; i++ ) 
				            sum += Math.pow(Math.log((Double)data[i]), 2);
				        double result;
				        if(numValues <= 1) {
				        	result = 0;
				        } else {
				        	result = Math.abs(Math.exp(Math.sqrt(sum/(numValues-1) - ((numValues/(numValues-1))*Math.pow(Math.log(values),2)))));
				        }
				        
						displaypanelLock.lock();
						if(Double.isNaN(result))
							mathPanel.labels.get(3).setText("-");
						else
							mathPanel.labels.get(3).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[3].start();

				//Thread updating harmonic mean
				workerThreads[4] = new Thread() {
					public void run() {
						double sum = 0;
						int numValues = data.length;
						for(int i = 0; i < numValues; i++) {
							sum += (double)1/(Double)data[i];
						}

						double result = (double)numValues/sum;
						displaypanelLock.lock();
						mathPanel.labels.get(4).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[4].start();

				//Thread updating maximum value
				workerThreads[5] = new Thread() {
					public void run() {
						double result = data[0];
						for(int i=0; i<data.length; i++) {
							double value = data[i];

							if(value > result)
								result = value;
						}

						displaypanelLock.lock();
						mathPanel.labels.get(5).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[5].start();

				//Thread updating minimum value
				workerThreads[6] = new Thread() {
					public void run() {
						double result = data[0];
						for(int i=0; i<data.length; i++) {
							double value = data[i];

							if(value < result)
								result = value;
						}

						displaypanelLock.lock();
						mathPanel.labels.get(6).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[6].start();

				//Thread updating bayesian confidence interval
				workerThreads[7] = new Thread() {
					public void run() {
						Arrays.sort(data);

						double values 		= 0;

						for(int i = 0; i < data.length; i++)
							values			+= (Double)data[i];

						double aritMean 	= values/data.length;
						double comp;
						double nearest		=(Double)data[0];
						int equalStart 		= 0;
						int equalEnd 		= 0;
						int tempHolder;

						for(int i = 0; i < data.length-1; i++) {
							comp = Math.abs(aritMean-nearest) - Math.abs(aritMean-(Double)data[i+1]);
							if(comp > 0) {
								nearest	 	= (Double)data[i+1]; 
								equalStart 	= i+1;
							}
							if(comp == 0) 
								equalEnd 	= i+1;
						}
						if(equalEnd == 0)
							tempHolder 		= equalStart;
						else
							tempHolder 		= equalStart + (equalEnd - equalStart)/2;

						double[] start 		= {nearest, tempHolder};
						int intervalLength 	= (int) ((double)(data.length)*(confidencelevel));
						int startPos 		= (int)start[1]; 
						int leftIndex 		= startPos-1;
						int rightIndex 		= startPos+1;
						double startNum 	= start[0];
						double tempHolder1;
						double tempHolder2;

						if(data.length == 0) {
							tempHolder1 	= Double.NaN;
							tempHolder2 	= Double.NaN;
							double[] result = {tempHolder1, tempHolder2};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(result[0] + " ; " + result[1]));
							displaypanelLock.unlock();
						} else if(data.length == 1) {
							tempHolder1 	= (Double) data[0];
							tempHolder2 	= (Double) data[0];
							double[] result = {tempHolder1, tempHolder2};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(formatter1.format(result[0]) + " ; " + formatter1.format(result[1])));
							displaypanelLock.unlock();
						} else if(data.length == 2) {
							tempHolder1 	= (Double) data[0];
							tempHolder2 	= (Double) data[1];
							double[] result = {tempHolder1, tempHolder2};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(formatter1.format(result[0]) + " ; " + formatter1.format(result[1])));
							displaypanelLock.unlock();
						} else {
							for(int i = 0 ; i < intervalLength ; i++) {
								if(leftIndex == 0)
									if(rightIndex < data.length-1)
										rightIndex++;

								if(rightIndex == data.length-1)
									if(leftIndex > 0)
										leftIndex--;

								if(leftIndex > 0 && Math.abs((Double)data[leftIndex] - startNum) <= Math.abs((Double)data[rightIndex] - startNum))
									leftIndex--;
								else if(rightIndex < data.length-1 && Math.abs((Double)data[leftIndex] - startNum) > Math.abs((Double)data[rightIndex] - startNum))
									rightIndex++;
							}
							double[] result = {(Double) data[leftIndex], (Double) data[rightIndex]};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(formatter1.format(result[0]) + " ; " + formatter1.format(result[1])));
							displaypanelLock.unlock();
						}
					}
				};
				workerThreads[7].start();	

				if(numPoints1 < 100) {
					geweke 					= -2;
					ess 					= -1;
					gelmanRubin				= false;
				} else {
					ess 					= MCMCMath.calculateESSmax(serie1);
					geweke 					= MCMCMath.calculateGeweke(serie1);
					gelmanRubin 			= MCMCMath.GelmanRubinTest(serie1, numBurnInPoints1);
				}

				burninPanel.getValueLabel(0).setText(String.valueOf(numBurnInPoints1));
				if (ess != -1)
					burninPanel.getValueLabel(2).setText(String.valueOf(ess));
				else
					burninPanel.getValueLabel(2).setText(String.valueOf("Short Input"));

				if (geweke == -2)
					burninPanel.getValueLabel(3).setText(String.valueOf("Short Input"));
				else if(geweke == -1)
					burninPanel.getValueLabel(3).setText(String.valueOf("Not Converged"));
				else
					burninPanel.getValueLabel(3).setText(String.valueOf(geweke));

				burninPanel.getValueLabel(4).setText(String.valueOf(gelmanRubin));
				burninField.setText(String.valueOf(numBurnInPoints1));

				if(numPoints2 < 100) {
					geweke1 				= -2;
					ess1 					= -1;
					gelmanRubin1			= false;
				} else {
					ess1 					= MCMCMath.calculateESSmax(serie2);
					geweke1 				= MCMCMath.calculateGeweke(serie2);
					gelmanRubin1 			= MCMCMath.GelmanRubinTest(serie2, numBurnInPoints2);
				}

				burninPanel2.getValueLabel(0).setText(String.valueOf(numBurnInPoints2));
				if (ess1 != -1)
					burninPanel2.getValueLabel(1).setText(String.valueOf(ess1));
				else
					burninPanel2.getValueLabel(1).setText(String.valueOf("Short Input"));

				if (geweke1 == -2)
					burninPanel2.getValueLabel(2).setText(String.valueOf("Short Input"));
				else if(geweke1 == -1)
					burninPanel2.getValueLabel(2).setText(String.valueOf("Not Converged"));
				else
					burninPanel2.getValueLabel(2).setText(String.valueOf(geweke1));

				burninPanel2.getValueLabel(3).setText(String.valueOf(gelmanRubin1));
				updateWilcoxonRankPanel();
			} else {
				//If burn in includes all data points set the following default values:
				String[] values 			= {"0","0","0","0", "0", "0", "0",  String.valueOf(confidencelevel*100 + "%"), "0 ; 0"};
				mathPanel.update(values);

				burninPanel.getValueLabel(0).setText(String.valueOf(numPoints1));
				burninPanel2.getValueLabel(0).setText(String.valueOf(numPoints2));
				burninField.setText(String.valueOf(numPoints2));
			}
		}
	}
	
	public void updateWilcoxonRankPanel() {
		final NumberFormat formatter 	= new DecimalFormat("0.#####E0");
		final NumberFormat formatter1 	= new DecimalFormat("0.####");
		
		int numPoints1 					= container1.getValueSerie(seriesID).size();	
		int numBurnInPoints1 			= (int)(numPoints1*burnin);	
		
		int numPoints2 					= container2.getValueSerie(seriesID).size();	
		int numBurnInPoints2 			= (int)(numPoints2*burnin);
		
		int sampleSize 					= numPoints1 - numBurnInPoints1;
		int sample2Size 				= numPoints2 - numBurnInPoints2;
		
		wilcoxonRankPanel.getValueLabel(0).setText(String.valueOf(sampleSize));
		wilcoxonRankPanel.getValueLabel(1).setText(String.valueOf(sample2Size));
		
		final double[] sample1 	= new double[sampleSize];
		final double[] sample2 	= new double[sample2Size];
		List<Double> temp1 = container1.getValueSerie(seriesID);
		List<Double> temp2 = container2.getValueSerie(seriesID);
		
		for(int i = 0 ; i < sampleSize; i++) 
			sample1[i] = temp1.get(i + numBurnInPoints1);
		for(int i=0 ; i < sample2Size; i++) 
			sample2[i] = temp2.get(i + numBurnInPoints2);	
		
		MannWhitneyUTest mw  = new MannWhitneyUTest();
		double testStatistic = mw.mannWhitneyU(sample1, sample2);
		double pValue  = mw.mannWhitneyUTest(sample1, sample2);
		
		wilcoxonRankPanel.getValueLabel(2).setText(String.valueOf(formatter.format(testStatistic)));
		wilcoxonRankPanel.getValueLabel(3).setText(String.valueOf(formatter1.format(pValue)));	
		if(pValue < alpha )
			wilcoxonRankPanel.getValueLabel(4).setText("Diff distr.");
		else
			wilcoxonRankPanel.getValueLabel(4).setText("Same distr.");
	}

	public MCMCDisplayPanel getFilePanel() 		{return filePanel;}
	public MCMCDisplayPanel getMathPanel() 		{return mathPanel;}
	public MCMCDisplayPanel getBurnInPanel() 	{return burninPanel;}
	@SuppressWarnings("rawtypes")
	public JComboBox getDropList() 				{return droplist;}
	@SuppressWarnings("rawtypes")
	public JComboBox getAlphaDropList() 		{return droplist1;}
	public JTextField getBurnInField() 			{return burninField;}
	public MCMCGraphToolParallelPanel getGraphTool() 	{return graphtoolPanel;}
	public void setAlpha(double alpha) 			{this.alpha = alpha; }
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
