package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import org.forester1.archaeopteryx.PdfExporter;

import se.cbb.jprime.consensus.day.RobinsonFoulds;
import se.cbb.jprime.consensus.day.MDSPanel;
import se.cbb.jprime.io.NewickTree;
import se.cbb.jprime.io.NewickTreeReader;
import se.cbb.vmcmc.libs.MCMCTree;

import mdsj.MDSJ;

public class MCMCTreeAnalysisTab extends MCMCStandardTab{
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private static final long serialVersionUID = 1L;
	private JTable									table;
	private DefaultTableModel 						model;
	private MDSPanel 								mds;
	private ArrayList<TreeMap<Integer, MCMCTree>> 	treeMapList;
	private JScrollPane 							newickImageScrollPane;
	private boolean 								redraw;

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MCMCTreeAnalysisTab() {
		super();
		/* ******************** FUNCTION VARIABLES ******************************** */		
		JScrollPane 					scrollPane;
		JButton 						saveImageToSystem;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		treeMapList 					= new ArrayList<TreeMap<Integer, MCMCTree>>();
		saveImageToSystem				= new JButton("Save Tree Analysis Panel");
		model 							= new DefaultTableModel();
		newickImageScrollPane			= new JScrollPane();
		redraw 							= true;

		/* ******************** FUNCTION BODY ************************************* */		
		saveImageToSystem.setBackground(Color.WHITE);
		
		saveImageToSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveImageToFile();
			}
		});
		
		table = new JTable(model) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int rowIndex, int colIndex) {
		        return false;   //Disallow the editing of any cell
		    }
		};
		scrollPane = new JScrollPane(table);
		
		scrollPane.setToolTipText("Displays the trees traversed in the MCMC run and their posterior distribution");
		newickImageScrollPane.setToolTipText("Displays the cladogram of currently selected tree or consensus tree of selected trees, " +
				"if applicable, where Archeopteryx has been used to generate this image with the settings provided with this file");
				
		westpanel.setMinimumSize(new Dimension(220, 0));
		westpanel.setPreferredSize(new Dimension(220, 0));
		
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		westpanel.setLayout(new BoxLayout(westpanel  , BoxLayout.Y_AXIS));
		
		table.getTableHeader().setBackground(new Color(0xFFDDDDFF));
				
		newickImageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		newickImageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		westpanel.add(scrollPane);
		centerPanel.add(newickImageScrollPane);
		southPanel.add(Box.createHorizontalGlue());
		southPanel.add(saveImageToSystem);
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void updateTreeMaps() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		TreeMap<Integer, MCMCTree> 	treeMap;
		ArrayList<MCMCTree> 		serie;
		int 						numPoints;	
		int 						numBurnInPoints;
		MCMCTree[] 					data;
		MCMCTree 					tree;
		int 						key;
		MCMCTree 					uniqueTree;
		int 						numDuplicates;
		ArrayList<MCMCTree> 		uniqueserie;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		
		/* ******************** FUNCTION BODY ************************************* */
		if(redraw == true) {
			redraw = false;
			treeMapList.clear();

			treeMap = new TreeMap<Integer, MCMCTree>();

			serie = datacontainer.getTreeSerie(seriesID);
			numPoints = serie.size();	
			numBurnInPoints = (int)(numPoints*burnin);
			data = new MCMCTree[numPoints-numBurnInPoints];

			System.arraycopy(serie.toArray(), numBurnInPoints, data, 0, numPoints-numBurnInPoints);

			for(int j = 0; j < data.length; j++) {
				tree = data[j];
				key = tree.getKey();
				uniqueTree = treeMap.get(key);

				if(uniqueTree == null) {
					uniqueTree = new MCMCTree();
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
					uniqueTree.setData(tree.getData());
					treeMap.put(key, uniqueTree);
				} else {
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
				}
			}

			uniqueserie = new java.util.ArrayList<MCMCTree>(treeMap.values());
			Collections.sort(uniqueserie);
			TreeMap<Integer, MCMCTree> tempMap = new TreeMap<Integer, MCMCTree>();
			treeMapList.add(tempMap);
			String newickTrees = "";

			for(int i = 0; i < uniqueserie.size(); i++) {
				uniqueTree 	= uniqueserie.get(i);
				numDuplicates = uniqueTree.getNumDuplicates();
				if( ((double) numDuplicates/(datacontainer.getNumLines()-(burnin*datacontainer.getNumLines()))) >= 0.002) {
					tempMap.put(i, uniqueTree);	
				} else
					break;
			}
			String[] labels = new String[tempMap.size()];
			double[] weights = new double[tempMap.size()];

			for(int i = 0; i < tempMap.size(); i++) {
				newickTrees = newickTrees + new String(tempMap.get(i).getData());
				labels[i] = "T" + i;
				weights[i] = tempMap.get(i).getNumDuplicates();
			}
			if(labels.length > 80) 
				JOptionPane.showMessageDialog(new JFrame(), "Warning for files with large trees and tree posteriors: The process takes time to construct 2D distance matrix. Please wait.");
			updateMDSPanel(newickTrees, labels, weights);
		}
		/* ******************** END OF FUNCTION *********************************** */
	}

	public void updateMDSPanel(String newickTrees, String[] labels, double[] weights) {
		List<NewickTree> trees;
		Dimension dim = this.getSize();
		try {
			trees = NewickTreeReader.readTrees(newickTrees, false);
			double[][] distMatrix = RobinsonFoulds.computeDistanceMatrix(trees, false);
			double[][] coords = MDSJ.classicalScaling(distMatrix); // apply MDS
			mds = new MDSPanel(labels, coords, weights, dim.width-222, dim.height-202);

			newickImageScrollPane.removeAll();
			newickImageScrollPane.add(mds);			
		} catch (Exception e) {
		}
	}
	
	/** Function for dealing with Tree parameter display in TreeAnalysis Tab for tree posterior. */
	public void updateTable() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final NumberFormat formatter = new DecimalFormat("0.00");
		TreeMap<Integer, MCMCTree> treeMap;
		ArrayList<MCMCTree> uniqueserie;
		MCMCTree uniqueTree;
		int numDuplicates;

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		treeMap = treeMapList.get(0);
		uniqueserie = new java.util.ArrayList<MCMCTree>(treeMap.values());
		model = new DefaultTableModel();
		
		/* ******************** FUNCTION BODY ************************************* */
		Collections.sort(uniqueserie);

		model.addColumn(datacontainer.getTreeNames().get(seriesID));
		model.addColumn("Duplicates n");
		model.addColumn("Tree Freq %");
		
		for(int i = 0; i < uniqueserie.size(); i++) {
			uniqueTree 	= uniqueserie.get(i);
			numDuplicates = uniqueTree.getNumDuplicates();
			Object[] rowdata = {"Tree " + i, numDuplicates, (formatter.format((double) numDuplicates/(datacontainer.getNumLines()-(burnin*datacontainer.getNumLines()))*100))};
			model.addRow(rowdata);
		}
		table.setModel(model);
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public JTable getTable() { return table;}
	public TreeMap<Integer, MCMCTree> getTreeMap(int index) { return treeMapList.get(index);}
	public void setredraw() { redraw = true; }
	
	public void saveImageToFile()  {
		JFileChooser fc = new JFileChooser();
	    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnVal = fc.showSaveDialog(MCMCTreeAnalysisTab.this);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
	    	File file = fc.getSelectedFile();
	    	try {
	    		PdfExporter.writeToPdf( file.getAbsolutePath(), this.mds, this.mds.getWidth(), this.mds.getHeight() );
	    	}
	    	catch(Exception e)  {
	    		e.printStackTrace();
	    	}
	    }
	}

	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
