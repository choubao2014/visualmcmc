package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.forester1.archaeopteryx.AptxUtil;
import org.forester1.archaeopteryx.Configuration;
import org.forester1.archaeopteryx.Constants;
import org.forester1.archaeopteryx.MainFrameApplication;
import org.forester1.archaeopteryx.MainPanel;
import org.forester1.archaeopteryx.PdfExporter;
import org.forester1.io.parsers.PhylogenyParser;
import org.forester1.io.parsers.nexus.NexusPhylogeniesParser;
import org.forester1.io.parsers.nhx.NHXParser;
import org.forester1.io.parsers.util.ParserUtils;
import org.forester1.phylogeny.Phylogeny;
import org.forester1.phylogeny.PhylogenyMethods;
import org.forester1.util.ForesterUtil;

import se.cbb.vmcmc.consensustree.ConsensusTree;
import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCTree;
import se.cbb.vmcmc.libs.MCMCTreeParamTesting;
import se.cbb.vmcmc.libs.SequenceFileWriter;
import se.cbb.vmcmc.newickhandler.NewickException;

/**
 * MCMCTreeTab: Tab panel for the tree tab.
 */
public class MCMCTreeParallelTab extends MCMCStandardTab {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private static final long 						serialVersionUID = 1L;
	private JTable									table;
	private DefaultTableModel 						model;
	private MainPanel								panelTree;
	private JLabel 									treelabel;
	@SuppressWarnings("rawtypes")
	private JComboBox								droplist;
	private JTextArea								newickTextArea;
	private ArrayList<TreeMap<Integer, MCMCTree>> 	treeMapList;
	private int 									treeIndex;
	private JScrollPane 							newickImageScrollPane;
	private Configuration							conf;	
	private MCMCDisplayPanel 						chiSquarePanel;
	@SuppressWarnings("rawtypes")
	private JComboBox 								droplist1;
	private MCMCDataContainer 						container1;
	private MCMCDataContainer 						container2;
	private double 									alpha;

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	@SuppressWarnings("rawtypes")
	public MCMCTreeParallelTab(final MCMCWindow parent) {
		super();
		/* ******************** FUNCTION VARIABLES ******************************** */		
		JScrollPane 					newickScrollPane;
		JButton 						createConsensus;
		JButton 						saveImageToSystem;
		JScrollPane 					scrollPane;
		JPanel 							panel;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		treeMapList 					= new ArrayList<TreeMap<Integer, MCMCTree>>();
		droplist 						= new JComboBox();
		panelTree 						= new MainPanel();
		createConsensus 				= new JButton("Create Consensus Tree");
		saveImageToSystem				= new JButton("Save Tree Figure");
		model 							= new DefaultTableModel();
		newickTextArea 					= new JTextArea();
		newickScrollPane 				= new JScrollPane(newickTextArea);
		newickImageScrollPane			= new JScrollPane();
		String[] chiSquare = {"Size of Sample1: ", "Size of Sample2: ", "Test statistic: ", "p-Value:", "Decision:"};
		JComponent[] chiSquarecomponents = {new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};
		panel = new JPanel();
		this.container1 = parent.getDataContainer(1);
		this.container2 = parent.getDataContainer(2);
		alpha = 0.05;
		
		/* ******************** FUNCTION BODY ************************************* */
		createConsensus.setToolTipText("Create Consensus Tree for the selected trees using the majority consensus tree,\n " +
				"if it is more than 50% of the selected trees");
		newickTextArea.setToolTipText("Newick or New Hampton formatted textual representation of the selected tree(s)");
		
		InputStream is = this.getClass().getResourceAsStream("/_aptx_configuration_file.txt");
    	if(is.equals(null)) {
    		System.out.println("File not found");
    		System.exit(-1);
    	}
    	try {
    		File f1 = new File("_aptx_configuration_file.txt");
    		OutputStream out=new FileOutputStream(f1);
    		byte buf[]=new byte[1024];
    		int len;
    		while((len=is.read(buf))>0)
    			out.write(buf,0,len);
    		out.close();
    		is.close();
    	} catch (IOException e){
    		System.err.println("Warning: Forester Configuration File not found. Using default configurations.");
    	}
		
    	conf = new Configuration( "_aptx_configuration_file.txt", false, false, true );
    	SequenceFileWriter.FileDeleter("_aptx_configuration_file.txt");
    	
    	newickImageScrollPane.setToolTipText("Displays the cladogram of currently selected tree or consensus tree of selected trees, " +
				"if applicable, where Archeopteryx has been used to generate this image with the settings provided with this file");
		
		treelabel = new JLabel();
		treelabel.setToolTipText("Label of selected tree from the table of trees");
		
		westpanel.setMinimumSize(new Dimension(250, 0));
		westpanel.setPreferredSize(new Dimension(250, 0));
		southPanel.setMinimumSize(new Dimension(0  , 50));
		southPanel.setPreferredSize(new Dimension(0  , 50));
		northPanel.setPreferredSize(new Dimension(0  , 100));
		
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		southPanel.setLayout(new GridBagLayout());

		droplist.setBackground(Color.WHITE);
		panelTree.setBackground(Color.WHITE);
		createConsensus.setBackground(Color.WHITE);
		saveImageToSystem.setBackground(Color.WHITE);
		
		createConsensus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculateConsensusTree();
			}
		});
		
		saveImageToSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveImageToFile();
			}
		});

		//Droplist actionlistener used to change tree serie selected in tree tab
		droplist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seriesID = droplist.getSelectedIndex();
				parent.getDataContainer(1).setNumTreeSeries(seriesID);
				updateTable();
			}
		});
		
		table = new JTable(model) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int rowIndex, int colIndex) {
		        return false;   //Disallow the editing of any cell
		    }
		};		
		
		table.getTableHeader().setBackground(new Color(0xFFDDDDFF));
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					int[] rowindices 		= table.getSelectedRows();
					String newick;
					ArrayList<MCMCTree> uniqueTreeList;

					newick = "";
					uniqueTreeList = new java.util.ArrayList<MCMCTree>(treeMapList.get(seriesID).values());
					Collections.sort(uniqueTreeList);

					for(int i = 0; i < rowindices.length; i++) 
						newick += new String(uniqueTreeList.get(rowindices[i]).getData()) + "\n";

					if(rowindices.length > 0)
						treeIndex = rowindices[0];

					newickTextArea.setText(newick);		

					updateImage(newick);
					newickImageScrollPane.removeAll();
					newickImageScrollPane.add(panelTree);
					centerPanel.setEnabled(true);
					centerPanel.setVisible(true);
				}
			}
		});
		
		scrollPane = new JScrollPane(table);
		scrollPane.setToolTipText("Displays the trees traversed in the MCMC run and their posterior distribution");
		
		newickTextArea.setEditable(false);
		newickScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		newickScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
				
		newickImageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		newickImageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		chiSquarePanel = new MCMCDisplayPanel("Chi Square test");
		chiSquarePanel.setToolTipText("Categorial two-sample Chi Square test for checking if both tree parameters have been sampled from the same distribution.");
		
		droplist1 = new JComboBox();
		droplist1.setBackground(Color.WHITE);
		
		chiSquarePanel.add(droplist1);
		chiSquarePanel.addComponents(chiSquare, chiSquarecomponents, new Dimension(250, 15));
		
		chiSquarePanel.setPreferredSize(new Dimension(250, 150));
		
		panel.add(chiSquarePanel);
		panel.setPreferredSize(new Dimension(250, 158));
		
		southPanel.add(Box.createHorizontalGlue());
		southPanel.add(createConsensus);
		southPanel.add(saveImageToSystem);
		southPanel.add(Box.createRigidArea(new Dimension(10, 0)));

		newickImageScrollPane.add(panelTree);
		
		westpanel.add(Box.createVerticalGlue());
		westpanel.add(panel);
		westpanel.add(scrollPane);
		
		centerPanel.add(newickImageScrollPane);
		northPanel.add(newickScrollPane);
				
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void saveImageToFile()  {
		JFileChooser fc = new JFileChooser();
	    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnVal = fc.showSaveDialog(MCMCTreeParallelTab.this);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
	    	File file = fc.getSelectedFile();
	    	try {
	    		PdfExporter.writeToPdf( file.getAbsolutePath(), this.panelTree, this.panelTree.getWidth(), this.panelTree.getHeight() );
	    	} catch(Exception e)  {
	    		e.printStackTrace();
	    	}
	    }
	}
	
	/**
	 * calculateConsensusTree: Uses library MCMCConsensusTree to find consensus tree for
	 * currently selected tree series with current burn in. Result is then displayed in
	 * the tabs JTree structure.
	 */
	public void calculateConsensusTree() {		
		/* ******************** FUNCTION VARIABLES ******************************** */
		ArrayList<MCMCTree> 	uniqueTreeList;
		ArrayList<MCMCTree> 	uniqueTreeListForIndex;
		JLabel 					noConsensusLabel;
		String 					newickString = "";

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		uniqueTreeList 		= new java.util.ArrayList<MCMCTree>(treeMapList.get(seriesID).values());
		if(table.getSelectedRowCount() <= 2) {
			uniqueTreeListForIndex = uniqueTreeList;
		} else {
			int[] rowindices 	= table.getSelectedRows();
			Collections.sort(uniqueTreeList);
			uniqueTreeListForIndex = new java.util.ArrayList<MCMCTree>();
			for(int i = 0; i < rowindices.length; i++) 
				uniqueTreeListForIndex.add(uniqueTreeList.get(rowindices[i]));
		}
		
		ArrayList<String> newickStrings = new ArrayList<String>();
		ArrayList<Integer> frequencies = new ArrayList<Integer>();
		for(int i = 0; i < uniqueTreeListForIndex.size(); i++) {
			newickStrings.add(new String(uniqueTreeListForIndex.get(i).getData()));
			frequencies.add(uniqueTreeListForIndex.get(i).getNumDuplicates());
		}
		
		try {
			newickString = ConsensusTree.consensustree (newickStrings, frequencies);
		} catch (NewickException e) {
			
		}
		
		if(newickString != "") {
			treelabel		.setText		("Consensus tree for serie " + datacontainer.getTreeNames().get(seriesID));
			newickTextArea	.setText		(newickString + "\n");
			updateImage(newickString);
			newickImageScrollPane.removeAll();
			newickImageScrollPane.add(panelTree);
		} else { 
			noConsensusLabel 		= new JLabel("No consensus tree available");
			noConsensusLabel	.setFont	(new Font("arial", Font.BOLD, 24));

			panelTree	.removeAll	();
			panelTree	.add		(noConsensusLabel);
			treelabel	.setText	("Consensus tree for serie " + datacontainer.getTreeNames().get(seriesID));
			panelTree	.revalidate	();
		}
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public void updateImage(String newick) {
		Dimension dim = this.getSize();
		if(newick == null || newick.equals("") || newick.equals(";"))
			return;
        try {
        	SequenceFileWriter.createAndWriteString("temp.txt", newick);
			
	        Phylogeny[] phylogenies = null;
	        File f = null;
	        try {
	        	f = new File("temp.txt");
	        	final String err = ForesterUtil.isReadableFile( f );
	        	if ( !ForesterUtil.isEmpty( err ) ) {
	        		ForesterUtil.fatalError( Constants.PRG_NAME, err );
	        	}
	        	final PhylogenyParser p = ParserUtils.createParserDependingOnFileType( f, conf
	        			.isValidatePhyloXmlAgainstSchema() );
	        	if ( p instanceof NHXParser ) {
	        		final NHXParser nhx = ( NHXParser ) p;
	        		nhx.setReplaceUnderscores( conf.isReplaceUnderscoresInNhParsing() );
	        		nhx.setIgnoreQuotes( false );
	        		nhx.setTaxonomyExtraction( conf.getTaxonomyExtraction() );
	        	}
	        	else if ( p instanceof NexusPhylogeniesParser ) {
	        		final NexusPhylogeniesParser nex = ( NexusPhylogeniesParser ) p;
	        		nex.setReplaceUnderscores( conf.isReplaceUnderscoresInNhParsing() );
	        		nex.setIgnoreQuotes( false );
	        	}
	        	phylogenies = PhylogenyMethods.readPhylogenies( p, f );
	        }
	        catch ( final Exception e ) {
	            ForesterUtil.fatalError( Constants.PRG_NAME, "failed to start: " + e.getLocalizedMessage() );
	        }
	        try {
				MainFrameApplication frame = new MainFrameApplication( phylogenies, conf, dim.width-250, dim.height-130, "Tree " + treeIndex);
				panelTree = frame.getMainPanel();
				frame.setVisible(false);
		        frame.dispose();
	        }
	        catch ( final OutOfMemoryError e ) {
	            AptxUtil.outOfMemoryError( e );
	        }
	        catch ( final Exception e ) {
	            AptxUtil.unexpectedException( e );
	        }
	        catch ( final Error e ) {
	            AptxUtil.unexpectedError( e );
	        }
        	treelabel.setText("Tree " + treeIndex);
        	SequenceFileWriter.FileDeleter("temp.txt");
        } catch (Exception e) {
        	System.out.println(e.getMessage());
        	System.exit(-1);
        }
	}
		
	public void updateTreeMaps() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		TreeMap<Integer, MCMCTree> 	treeMap;
		ArrayList<MCMCTree> 		serie;
		int 						numPoints;	
		int 						numBurnInPoints;
		MCMCTree[] 					data;
		MCMCTree 					tree;
		int 						key;
		MCMCTree 					uniqueTree;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		
		/* ******************** FUNCTION BODY ************************************* */
		treeMapList.clear();
		
		for(int i = 0; i < datacontainer.getNumTreeSeries(); i++) {
			treeMap = new TreeMap<Integer, MCMCTree>();
			treeMapList.add(treeMap);
			
			serie = datacontainer.getTreeSerie(i);
			numPoints = serie.size();
			numBurnInPoints = (int)(numPoints*burnin);

			data = new MCMCTree[numPoints-numBurnInPoints];

			System.arraycopy(serie.toArray(), numBurnInPoints, data, 0, numPoints-numBurnInPoints);

			for(int j = 0; j < data.length; j++) {
				tree = data[j];
				key = tree.getKey();
				uniqueTree = treeMap.get(key);

				if(uniqueTree == null) {
					uniqueTree = new MCMCTree();
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
					uniqueTree.setData(tree.getData());
					treeMap.put(key, uniqueTree);
				} else {
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
				}
			}
		}
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	public void updateTable() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final NumberFormat formatter = new DecimalFormat("0.00");
		TreeMap<Integer, MCMCTree> treeMap;
		ArrayList<MCMCTree> uniqueserie;
		MCMCTree uniqueTree;
		int numDuplicates;

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		treeMap = treeMapList.get(seriesID);
		uniqueserie = new java.util.ArrayList<MCMCTree>(treeMap.values());
		model = new DefaultTableModel();
		
		/* ******************** FUNCTION BODY ************************************* */
		Collections.sort(uniqueserie);

		model.addColumn(datacontainer.getTreeNames().get(seriesID));
		model.addColumn("Duplicates n");
		model.addColumn("Tree Freq %");
		
		for(int i = 0; i < uniqueserie.size(); i++) {
			uniqueTree 	= uniqueserie.get(i);
			numDuplicates = uniqueTree.getNumDuplicates();
			Object[] rowdata = {"Tree " + i, numDuplicates, (formatter.format((double) numDuplicates/(datacontainer.getNumLines()-(burnin*datacontainer.getNumLines()))*100))};
			
			model.addRow(rowdata);
		}
		table.setModel(model);
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public void updateChiSquarePanel(final MCMCWindow parent) {
		final NumberFormat formatter = new DecimalFormat("0.#####E0");
		final NumberFormat formatter1 = new DecimalFormat("0.####");
		int numPoints1 				= parent.getDataContainer(1).getValueSerie(seriesID).size();	
		int numBurnInPoints1 		= (int)(numPoints1*burnin);	
		int numPoints2 				= container2.getValueSerie(seriesID).size();	
		int numBurnInPoints2 		= (int)(numPoints2*burnin);
		int sampleSize = numPoints1 - numBurnInPoints1;
		chiSquarePanel.getValueLabel(0).setText(String.valueOf(sampleSize));
		int sample2Size = numPoints2 - numBurnInPoints2;
		chiSquarePanel.getValueLabel(1).setText(String.valueOf(sample2Size));
		
		ArrayList<String> series1 = new ArrayList<String>();
		ArrayList<String> series2 = new ArrayList<String>();
		
		for(int i = 0; i < container1.getTreeSerie(container1.getSelectedTreeIndex()).size(); i++)
			series1.add(new String(container1.getTreeSerie(container1.getSelectedTreeIndex()).get(i).getData()));
		for(int i = 0; i < container2.getTreeSerie(container2.getSelectedTreeIndex()).size(); i++)
			series2.add(new String(container2.getTreeSerie(container2.getSelectedTreeIndex()).get(i).getData()));
		
/*		try {for(int i = 0 ; i < series1.size(); i++)
			SequenceFileWriter.writeAndAppendLine("/Users/rhali/Desktop/", "Treetemp.txt", series1.get(i));
		for(int i = 0 ; i < series2.size(); i++) 
			SequenceFileWriter.writeAndAppendLine("/Users/rhali/Desktop/", "Treetemp2.txt", series2.get(i));
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
*/		ArrayList<String> res = MCMCTreeParamTesting.performTest(series1, series2, numBurnInPoints1, numBurnInPoints2, alpha, true);
		double testStatistic = Double.parseDouble(res.get(0)); 
		double pValue = Double.parseDouble(res.get(1)); 
		
		chiSquarePanel.getValueLabel(2).setText(String.valueOf(formatter.format(testStatistic)));
		chiSquarePanel.getValueLabel(3).setText(String.valueOf(formatter1.format(pValue)));	
		if(pValue < alpha )
			chiSquarePanel.getValueLabel(4).setText("Diff. populations");
		else
			chiSquarePanel.getValueLabel(4).setText("Same population");
	}
	
	@SuppressWarnings("rawtypes")
	public JComboBox getDropList() {return droplist;}
	@SuppressWarnings("rawtypes")
	public JComboBox getAlphaDropList() 		{return droplist1;}
	public JTable getTable() {return table;}
	public TreeMap<Integer, MCMCTree> getTreeMap(int index) {return treeMapList.get(index);}
	public void setAlpha(double alpha) 			{this.alpha = alpha; }
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
