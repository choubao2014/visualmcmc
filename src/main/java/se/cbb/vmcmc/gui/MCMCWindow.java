package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.SequenceFileWriter;
/**
 * MCMCWindow: JFrame with default layout implemented. Stores a JTabbedPane containing each
 * file openened.
 */
public class MCMCWindow extends JFrame {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private static final long serialVersionUID = 1L;
	private JTabbedPane fileTabs;
	private JPanel defaultpanel;
	private JPanel mainpanel;
	private MCMCDataContainer container;
	private MCMCDataContainer container1;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MCMCWindow() {
		InputStream is = this.getClass().getResourceAsStream("/c.png");
		try {
			this.setIconImage(ImageIO.read(is));
		} catch (IOException e) {
			System.err.println("Warning: \"res directory and image files not found\"");
		}
		
		mainpanel = new JPanel();
		mainpanel.setLayout(new BoxLayout(mainpanel, BoxLayout.X_AXIS));
		mainpanel.setBackground(new Color(0xFFEEEEFF));
		
		defaultpanel = new JPanel();
		
		defaultpanel.setLayout(new BoxLayout(defaultpanel, BoxLayout.X_AXIS));
		defaultpanel.setBackground(new Color(0xFFEEEEFF));
		
		fileTabs = new JTabbedPane();
		fileTabs.setBackground(new Color(0xFFEEFFEE));
		fileTabs.setFocusable(false);
		fileTabs.setVisible(false);
		
		defaultpanel.add(fileTabs);
		mainpanel.add(defaultpanel);
		
		this.setContentPane(defaultpanel);
		this.setSize(new Dimension(700, 240));
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
		
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void addTab(String title, Component panel) {
		fileTabs.add(title, panel);
		this.pack();
	}
	public void appear() {fileTabs.setVisible(true);}
	public void windowAppear() {this.setVisible(true);}
	public void selectTab(int index) {fileTabs.setSelectedIndex(index);}
	public void getTab(int index) {fileTabs.getComponentAt(index);}
	public JTabbedPane getTabs() {return fileTabs;}
		
	public void setDataContainer(int index, MCMCDataContainer dc) {
		if(index == 1)
			container = dc;
		else
			container1 = dc;
	}
	
	public MCMCDataContainer getDataContainer(int index) {
		if(index == 1)
			return container;
		else
			return container1;
	}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}

