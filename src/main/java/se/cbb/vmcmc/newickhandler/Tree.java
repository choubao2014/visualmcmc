package se.cbb.vmcmc.newickhandler;

import java.util.ArrayList;
import java.util.BitSet;

public class Tree {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private Node root;
	private String newick;
	private int frequency;
	private ArrayList<BitSet> splitsList; 
	private ArrayList<Integer> freqList;
	private ArrayList<String> leafMap;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public Tree(String newick) throws NewickException {
		int colon = newick.lastIndexOf(':');
		int bracketcounter = 0;
		for (int i = 0; i < newick.length(); i++) {
		    if (newick.charAt(i) == '(')
		    	bracketcounter++;
		    else if (newick.charAt(i) == ')')
			   	bracketcounter--;
			if(bracketcounter < 0)
				throw new NewickException("Improper newick format : At least one \")\" bracket precedes its \"(\" bracket."); 
		}
		if(bracketcounter != 0)
			throw new NewickException("Improper newick format : The number of \"(\" brackets and \")\" brackets are not equal for string \n" + newick);
		if(newick.endsWith(";")) 
			newick = newick.substring(0, newick.length() - 1);
		this.newick = newick;
		if(colon == -1) 
			root = buildWithoutLength(newick, new Node(), 0, newick.length());
		else
			root = buildWithLength(newick, new Node(newick.substring(colon+1)), 0, colon);
		if(root == null)
			throw new NewickException("Can not read this string. Either improper newick or the tre contains internal node names and/or comments.");
	}
	
	public Tree(Node root) {
		this.root = root;
		newick = root.toString();
	}
	
	public Tree(Node root, boolean consensus) {
		this.root = root;
		newick = root.toStringWithSupportVal();
	}
	
	public Tree(String newick, int frequency) throws NewickException {
		this.frequency = frequency;
		int colon = newick.lastIndexOf(':');
		int bracketcounter = 0;
		for (int i = 0; i < newick.length(); i++) {
		    if (newick.charAt(i) == '(')
		    	bracketcounter++;
		    else if (newick.charAt(i) == ')')
			   	bracketcounter--;
			if(bracketcounter < 0)
				throw new NewickException("Improper newick format : At least one \")\" bracket precedes its \"(\" bracket."); 
		}
		if(bracketcounter != 0)
			throw new NewickException("Improper newick format : The number of \"(\" brackets and \")\" brackets are not equal for string \n" + newick);
		if(newick.endsWith(";")) 
			newick = newick.substring(0, newick.length() - 1);
		this.newick = newick;
		if(colon == -1) 
			root = buildWithoutLength(newick, new Node(), 0, newick.length());
		else
			root = buildWithLength(newick, new Node(newick.substring(colon+1)), 0, colon);
		if(root == null)
			throw new NewickException("Can not read this string. Either improper newick or the tre contains internal node names and/or comments.");
	}
	
	public Tree(Node root, int frequency) {
		this.root = root;
		newick = root.toString();
		this.frequency = frequency;
	}
	
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */	
	private Node buildWithLength(String newickStringWithLengths, Node parent, int from, int to) {
		if (newickStringWithLengths.charAt(from) != '(') {
			parent.setName(newickStringWithLengths.substring(from, to));
			return parent;
		}

		int bracketCounter = 0;
		int colon = 0; // colon marker
		int positionMarker = from; // position marker

		for (int i = from; i < to; i++) {
			char c = newickStringWithLengths.charAt(i);

			if (c == '(')
				bracketCounter ++;
			else if (c == ')')
				bracketCounter --;
			else if (c == ':')
				colon = i;

			if (bracketCounter == 0 || bracketCounter == 1 && c == ',') {
				parent.addChild(buildWithLength(newickStringWithLengths, new Node(newickStringWithLengths.substring(colon+1, i)), positionMarker + 1, colon));
				positionMarker = i;
			}
		}
		return parent;
	}
	
	private Node buildWithoutLength(String newickWithoutLength, Node parent, int from, int to) {
		if (newickWithoutLength.charAt(from) != '(') {
			parent.setName(newickWithoutLength.substring(from, to));
			return parent;
		}

		int bracketCounter = 0;
		int positionMarker = from; // position marker

		for (int i = from; i < to; i++) {
			char c = newickWithoutLength.charAt(i);

			if (c == '(')
				bracketCounter ++;
			else if (c == ')')
				bracketCounter --;

			if (bracketCounter == 0 || bracketCounter == 1 && c == ',') {
				parent.addChild(buildWithoutLength(newickWithoutLength, new Node(), positionMarker + 1, i));
				positionMarker = i;
			}
		}
		return parent;
	}
	
	private void computeSplits(Node node, ArrayList<BitSet> splits, ArrayList<Integer> frequencies) {
		if(node.numChildren() == 0) {
			BitSet split = new BitSet(leafMap.size());
			split.flip(getIndex(node.getName()));
			node.setSplitName(split);
			splits.add(split);
			frequencies.add(frequency);
		} else {
			BitSet split = new BitSet(leafMap.size());
			for(int i = 0; i < node.numChildren(); i++) {
				computeSplits(node.getChild(i),splits,frequencies);
				split.or(node.getChild(i).getSplitName());
			}
			node.setSplitName(split);
			splits.add(split);
			frequencies.add(frequency);
		}
	}
	
	private void generateLeafMap(Node node) {
		if(node.numChildren() == 0) {
			leafMap.add(node.getName());
		} else {
			generateLeafMap(node.getChild(0));
			generateLeafMap(node.getChild(1));
		}
	}
	
	private int getIndex(String name) {
		for(int i = 0; i < leafMap.size(); i++) {
			if(leafMap.get(i).equals(name))
				return i;
		}
		return -1;
	}
		
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public Node getTree() { return root; }
	public String getTreeInNewick() { return newick; }
	public ArrayList<BitSet> getSplitList(){ return splitsList;}
	public ArrayList<Integer> getFreqList(){ return freqList;}
	public void computeSplitList() {
		splitsList = new ArrayList<BitSet>();
		freqList = new ArrayList<Integer>();
		computeSplits(root, splitsList, freqList);
	}
	public void setleafMap(ArrayList<String> leafMap) { this.leafMap = leafMap; }

	public ArrayList<String> initializeSplitMap() {
		leafMap = new ArrayList<String>();
		generateLeafMap(root);
		return leafMap;
	}

	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
