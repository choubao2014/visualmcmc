package se.cbb.vmcmc.consensustree;

import java.util.ArrayList;
import java.util.BitSet;

import se.cbb.vmcmc.newickhandler.NewickException;
import se.cbb.vmcmc.newickhandler.Node;
import se.cbb.vmcmc.newickhandler.Tree;

public class ConsensusTree {

	public static String consensustree (ArrayList<String> trees, ArrayList<Integer> frequencies) throws NewickException {
		ArrayList<String> leafMap;
		ArrayList<BitSet> combinedSplitList;
		ArrayList<Integer> combinedFrequencyList;

		if(trees.size() > 0) {
			Tree tree1 = new Tree(trees.get(0), frequencies.get(0));
			leafMap = tree1.initializeSplitMap();
			tree1.computeSplitList();
			ArrayList<BitSet> splitList1 = tree1.getSplitList();
			ArrayList<Integer> freqList1 = tree1.getFreqList();

			combinedSplitList = splitList1;
			combinedFrequencyList = freqList1;
		} else {
			return null;
		}

		int totalSamples = 0;
		for(int i = 0; i < frequencies.size(); i++) {
			totalSamples = totalSamples + frequencies.get(i);
		}

		for(int i = 1; i < trees.size(); i++) {
			Tree tree2 = new Tree(trees.get(i), frequencies.get(i));

			tree2.setleafMap(leafMap);
			tree2.computeSplitList();

			ArrayList<BitSet> splitList2 = tree2.getSplitList();
			ArrayList<Integer> freqList2 = tree2.getFreqList();

			combineSplitLists(splitList2, freqList2, combinedSplitList, combinedFrequencyList);				
		}

		ArrayList<Double> supportList = convertfrequencytosupport(totalSamples, combinedFrequencyList);

		ArrayList<BitSet> supportedSplitList = new ArrayList<BitSet>();
		ArrayList<Double> supportedFrequencyList = new ArrayList<Double>();

		for(int i = 0 ; i < combinedSplitList.size(); i++) {
			if(supportList.get(i) > 0.5) {
				supportedSplitList.add(combinedSplitList.get(i));
				supportedFrequencyList.add(supportList.get(i));
			}
		}

		Node[] nodelist = new Node[supportedSplitList.size()];
		for(int i = 0; i < supportedSplitList.size(); i++) {
			nodelist[i] = new Node();
			nodelist[i].setSupportValue(supportedFrequencyList.get(i));
			if(supportedSplitList.get(i).cardinality() == 1) {
				nodelist[i].setName(leafMap.get(supportedSplitList.get(i).nextSetBit(0)));
			}
		}

		Node root = null;

		for(int i = 0; i < supportedSplitList.size(); i++) {
			ArrayList<Integer> candidate = new ArrayList<Integer>();
			for(int j = 0; j < supportedSplitList.size(); j++) { 
				if( i != j) {
					if(supportedSplitList.get(i).intersects(supportedSplitList.get(j)) && supportedSplitList.get(i).cardinality() < supportedSplitList.get(j).cardinality())
						candidate.add(j);
				}
			}

			if(supportedSplitList.get(i).cardinality() < leafMap.size()) {
				int index = 0;

				for(int j = 1; j < candidate.size(); j++) {
					if(supportedSplitList.get(candidate.get(index)).cardinality() > supportedSplitList.get(candidate.get(j)).cardinality()) {
						index = j;
					}
				}

				nodelist[candidate.get(index)].addChild(nodelist[i]);
				nodelist[candidate.get(index)].setName(nodelist[candidate.get(index)].getSupportValue().toString());
			} else {
				root = nodelist[i];
			}
		}

		if(root != null) {
			return new Tree(root,true).getTreeInNewick() + ";";
		}
		return null;
	}
	
	private static void combineSplitLists(ArrayList<BitSet> splitList2, ArrayList<Integer> freqList2, ArrayList<BitSet> resultsplitList, ArrayList<Integer> resultfreqList) {
		for(int i = 0; i < resultsplitList.size(); i++) {
			for(int j = 0; j < splitList2.size(); j++) {
				if(resultsplitList.get(i).equals(splitList2.get(j))) {
					resultfreqList.set(i, resultfreqList.get(i) + freqList2.get(j));
					splitList2.remove(j);
					freqList2.remove(j);
					break;
				}
			}
		}
		
		for(int j = 0; j < splitList2.size(); j++) {
			resultsplitList.add(splitList2.get(j));
			resultfreqList.add(freqList2.get(j));
		}
	}
	
	private static ArrayList<Double> convertfrequencytosupport(int totalfrequency, ArrayList<Integer> freqList) {
		ArrayList<Double> resultList = new ArrayList<Double>();
		for(int i = 0; i < freqList.size(); i++)
			resultList.add(round((double)freqList.get(i) / totalfrequency, 2));
		return resultList;
	}
	
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
}

