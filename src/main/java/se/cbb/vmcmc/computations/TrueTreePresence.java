package se.cbb.vmcmc.computations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import se.cbb.jprime.consensus.day.RobinsonFoulds;
import se.cbb.jprime.io.NewickTree;
import se.cbb.jprime.io.NewickTreeReader;
import se.cbb.vmcmc.libs.SequenceFileWriter;
import se.cbb.vmcmc.newickhandler.Tree;

public class TrueTreePresence {
	public static void computeTrueTreeTest(String path) {
		BufferedReader bufferedreader;
		String str;
		try {
			String path1 = path + "Posterior";
			File folder = new File(path1);
			File[] listOfFiles = folder.listFiles();
			int numberofFiles = listOfFiles.length;
			
			ArrayList<String> list = new ArrayList<String>();

			for(int i = 0; i < numberofFiles; i++) {
				if(listOfFiles[i].getName().endsWith(".pos25"))
					list.add(listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length()-6));
//					list.add(listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length()-6));
			}
			
/*			SequenceFileWriter.writeAndAppendLine(path, "PosteriorScript0.sh", "#!/bin/bash");
			SequenceFileWriter.writeAndAppendLine(path, "PosteriorScript0.sh", "");
			
			for(int i = 0; i < list.size(); i++) {
				SequenceFileWriter.writeAndAppendLine(path, "PosteriorScript0.sh", "java -Xmx2000m -Xms1800m -jar VMCMC-1.0.1-uber.jar -p " + list.get(i) + ".mcmc -o " + list.get(i) + ".pos -b 0");
			}*/
			for(int i = 0; i < list.size(); i++) {
				ArrayList<Float> posterior = new ArrayList<Float>();
				Float pos = (float) 0;
				posterior.add(pos);

				bufferedreader = new BufferedReader(new FileReader(path + "PrimeTrees/" + list.get(i) + ".nwk"));
				str = bufferedreader.readLine(); 
				bufferedreader.close();

/*				Tree newicktree = new Tree(str);
				se.cbb.vmcmc.newickhandler.Node root1 = newicktree.getTree();
				root1.sortChildrenAlphabetically();
				se.cbb.vmcmc.newickhandler.Node temp1 = root1.rerootTree(root1);
				
				String newickTree = temp1.toStringWithoutLength() + ";\n";*/
				
				String newickTree = str;
				System.out.println(list.get(i) + "\t" + newickTree);

				bufferedreader = new BufferedReader(new FileReader(path + "Posterior/" + list.get(i) + ".pos25"));

				int count = 0;
				while ((str = bufferedreader.readLine()) != null) {
					if(str.startsWith("\t\t\t\t\"Posterior probability\": ")) {
						count++;
						if(count < 1001) {
							String[] strArr = str.split(": ");
							Float temp = Float.parseFloat(strArr[1].substring(0, strArr[1].length()-1));
							pos = pos + temp;
							posterior.add(pos);

							str = bufferedreader.readLine();
							String[] strArr1 = str.split(": \"");
							newickTree = newickTree + strArr1[1].substring(0, strArr1[1].length()-1) + "\n";
						}
					}
				}
				bufferedreader.close();
				
				computeTrueTree(newickTree, path + "TrueTrees/", list.get(i) + ".dis", posterior);
				System.out.println(" ... Done");
			}
		} catch (Exception e) {
			System.out.println("Error in computeTrueTreeTest :" + e.getMessage());
		}
	}
	
	public static void equalityOfBothTrees(String path) {
		BufferedReader bufferedreader;
		String str;
		try {
			String path1 = path + "FastTree";
			File folder = new File(path1);
			File[] listOfFiles = folder.listFiles();
			int numberofFiles = listOfFiles.length;
			
			ArrayList<String> list = new ArrayList<String>();

			for(int i = 0; i < numberofFiles; i++) {
				if(listOfFiles[i].getName().endsWith(".nwk"))
					list.add(listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length()-4));
			}
			
			for(int i = 0; i < list.size(); i++) {
				bufferedreader = new BufferedReader(new FileReader(path + "PrimeTrees/" + list.get(i) + ".nwk"));
				str = bufferedreader.readLine(); 
				bufferedreader.close();
				
				String newickTree = str; 
				
				bufferedreader = new BufferedReader(new FileReader(path + "FastTree/" + list.get(i) + ".nwk"));
				str = bufferedreader.readLine(); 
				bufferedreader.close();
				

				Tree newicktree1 = new Tree(str);
				se.cbb.vmcmc.newickhandler.Node root1 = newicktree1.getTree();
				root1.sortChildrenAlphabetically();
				se.cbb.vmcmc.newickhandler.Node temp1 = root1.rerootTree(root1);
				
				newickTree += temp1.toStringWithoutLength() + ";\n";
				
				System.out.println(list.get(i) + "\t" + newickTree);

//				computeTrueTree(newickTree, path + "TrueTrees/", list.get(i) + ".dis", posterior);
				System.out.println(" ... Done");
			}
		} catch (Exception e) {
			System.out.println("Error in computeTrueTreeTest :" + e.getMessage());
		}
	}
	
	public static void computeTrueTree(String newickTrees, String path, String output, ArrayList<Float> posterior) {
		List<NewickTree> trees;
		
		try {
			trees = NewickTreeReader.readTrees(newickTrees, false);
			double[][] distMatrix = RobinsonFoulds.computeDistanceMatrix(trees, false);
			for(int j = 0; j < distMatrix.length; j++) {
				SequenceFileWriter.writeAndAppendString(path, output, distMatrix[0][j] + "\t");
			}	
			SequenceFileWriter.writeAndAppendLine(path, output, "");	
			SequenceFileWriter.writeAndAppendString(path, output, "0.0\t");	
			for(int j = 0; j < posterior.size(); j++) {
				SequenceFileWriter.writeAndAppendString(path, output, posterior.get(j) + "\t");
			}	
			SequenceFileWriter.writeAndAppendLine(path, output, "");	
			for(int j = 1; j < distMatrix.length; j++) {
				if(distMatrix[0][j] == 0.0) {
					SequenceFileWriter.writeAndAppendLine(path, output, j + "\t" + posterior.get(j) + "\t" + posterior.get(j - 1));
				}
			}	
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
		}
	}
	
	public static void generateScriptForPosterior(String path, String method) {
		System.out.println("I was herer");
		String extension = ".p";
		ArrayList<String> list = generateList(path + "Run1/", extension);
		String str;
		int counter = 0;
		String scriptfilename = "";
		if(method.equals("0")) {
			scriptfilename = "PosteriorScriptZero";
		} else if(method.equals("25")) {
			scriptfilename = "PosteriorScriptTwentyFive";
		} else if(method.equals("gelmanrubin")) {
			scriptfilename = "PosteriorScriptGelman.sh";
		} else if(method == "geweke") {
			scriptfilename = "PosteriorScriptGeweke.sh";
		} else if(method == "noburnin") {
			scriptfilename = "PosteriorScriptNoBurnin.sh";
		} else if(method == "25%") {
			scriptfilename = "PosteriorScript25Burnin.sh";
		} else if(method == "esslast") {
			scriptfilename = "PosteriorScriptEssLast.sh";
		} else if(method == "essnormalized") {
			scriptfilename = "PosteriorScriptEssNormalized.sh";
		} else if(method == "essstandardized") {
			scriptfilename = "PosteriorScriptEssStand.sh";
		}
			
		try {
			if(!method.equals("0") && !method.equals("25"))
				SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "#!/bin/bash\n");
			for(int i = 0; i < list.size(); i++) {
				try {
					if(method == "0") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + extension + " -p -b 0 -o " + list.get(i) + ".pos");
					} else if (method == "25") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + extension + " -p -b 2500 -o " + list.get(i) + ".pos25");
					} else {
						BufferedReader bufferedreader = new BufferedReader(new FileReader(path + "ConvergenceTest/Run1/" + list.get(i).substring(0, list.get(i).length()-9) + ".ct"));
						int count = 0;
						while ((str = bufferedreader.readLine()) != null) {
							count++;
							if(method == "gelmanrubin") {
								if(count == 9) {
									String[] strArr = str.split("Gelman_Rubin\": ");
//									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + extension + " -p -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".posgr");
								}
							} else if(method == "geweke") {
								if(count == 7) {
									String[] strArr = str.split("Geweke\": ");
//									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -p -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".posge");
								}
							} else if(method == "noburnin") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -p -b 0 -o " + list.get(i) + ".pos0");
							} else if(method == "25%") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -p -b 12500 -o " + list.get(i) + ".pos0");
							} else if(method == "esslast") {
								if(count == 8) {
									String[] strArr = str.split("ESS\": ");
//									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -p -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".posel");
								}
							} else if(method == "essnormalized") {
								if(count == 11) {
									String[] strArr = str.split("Normalized ESS\": ");
//									if(!strArr[1].equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -p -b " + strArr[1] + " -o " + list.get(i) + ".posen");
								}
							} else if(method == "essstandardized") {
								if(count == 10) {
									String[] strArr = str.split("Standardized ESS\": ");
//									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -p -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".poses");
								}
							}
						}
						bufferedreader.close();
					}
				} catch (Exception e) {
					System.out.println("Error in writing script : " + e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("Error in writing script : " + e.getMessage());
		}
	}
	
	public static void performParallelChainAnalysis(String path, String method) {
		String str;
		String scriptfilename = "";
		String ext = "";
		String folder = "";
		if(method.equals("0")) {
			scriptfilename = ".par0";
			folder = "Posterior0/";
			ext = ".pos";
		} else if(method.equals("25")) {
			scriptfilename = ".par25";
			folder = "Posterior25/";
			ext = ".pos25";
		} else if(method.equals("gelmanrubin")) {
			scriptfilename = ".pargr";
			folder = "PosteriorGelman/";
			ext = ".posgr";
		} else if(method == "geweke") {
			scriptfilename = ".parge";
			folder = "PosteriorGeweke/";
			ext = ".posge";
		} else if(method == "esslast") {
			scriptfilename = ".parel";
			folder = "PosteriorEssLast/";
			ext = ".posel";
		} else if(method == "essnormalized") {
			scriptfilename = ".paren";
			folder = "PosteriorEssNorm/";
			ext = ".posen";
		} else if(method == "essstandardized") {
			scriptfilename = ".pares";
			folder = "PosteriorEssStan/";
			ext = ".poses";
		}
		
		ArrayList<String> informedList = generateList(path + "Informed/" + folder, ext);
		ArrayList<String> uninformedList = generateList(path + "Uninformed/" + folder, ext);
		
		ArrayList<String> commonList = new ArrayList<String>();
		for(int i = 0; i < informedList.size(); i++) {
			for(int j = 0; j < uninformedList.size(); j++) {
				if(informedList.get(i).substring(8).equals(uninformedList.get(j))) {
					commonList.add(uninformedList.get(j));
					break;
				}
			}
		}
		
		try {
			for(int i = 0; i < commonList.size(); i++) {
				ArrayList<String> treeInformed = new ArrayList<String>(); 
				ArrayList<Integer> posteriorInformed = new ArrayList<Integer>(); 
				BufferedReader bufferedreader = new BufferedReader(new FileReader(path + "Informed/" + folder + "Informed" + commonList.get(i) + ext));
				
				while ((str = bufferedreader.readLine()) != null) {
					if(str.startsWith("\t\t\t\t\"Duplicates\"")) {
						posteriorInformed.add(Integer.parseInt(str.substring(18, str.length()-1)));
					}
					
					if(str.startsWith("\t\t\t\t\"Newick\"")) {
						treeInformed.add(str.substring(15, str.length()-1));
					}
				}
				bufferedreader.close();
				
				ArrayList<String> treeUninformed = new ArrayList<String>(); 
				ArrayList<Integer> posteriorUninformed = new ArrayList<Integer>(); 
				bufferedreader = new BufferedReader(new FileReader(path + "Uninformed/" + folder + commonList.get(i) + ext));
				
				while ((str = bufferedreader.readLine()) != null) {
					if(str.startsWith("\t\t\t\t\"Duplicates\"")) {
						posteriorUninformed.add(Integer.parseInt(str.substring(18, str.length()-1)));
					}
					
					if(str.startsWith("\t\t\t\t\"Newick\"")) {
						treeUninformed.add(str.substring(15, str.length()-1));
					}
				}
				bufferedreader.close();
				
				ArrayList<String> treeList = new ArrayList<String>(); 
				ArrayList<Integer> posUninformed = new ArrayList<Integer>(); 
				ArrayList<Integer> posInformed = new ArrayList<Integer>(); 
				for(int j = 0; j < treeInformed.size(); j++) {
					for(int k = 0; k < treeUninformed.size(); k++) {
						if(treeInformed.get(j).equals(treeUninformed.get(k))) {
							treeList.add(treeInformed.get(j));
							posInformed.add(posteriorInformed.get(j));
							posUninformed.add(posteriorUninformed.get(k));
							break;
						}
					}
				}
				
				for(int j = 0; j < treeInformed.size(); j++) {
					boolean exists = false;
					for(int k = 0; k < treeList.size(); k++) {
						if(treeInformed.get(j).equals(treeList.get(k))) {
							exists = true;
							break;
						}
					}
					if(exists == false) {
						treeList.add(treeInformed.get(j));
						posInformed.add(posteriorInformed.get(j));
						posUninformed.add(0);
					}
				}
				
				for(int j = 0; j < treeUninformed.size(); j++) {
					boolean exists = false;
					for(int k = 0; k < treeList.size(); k++) {
						if(treeUninformed.get(j).equals(treeList.get(k))) {
							exists = true;
							break;
						}
					}
					if(exists == false) {
						treeList.add(treeUninformed.get(j));
						posInformed.add(0);
						posUninformed.add(posteriorUninformed.get(j));
					}
				}
				
/*				for(int j = 0; j < treeList.size(); j++) {
					SequenceFileWriter.writeAndAppendLine(path, "TreeList.txt", commonList.get(i) + "\t" + treeList.get(j) + "\t" + posInformed.get(j) + "\t" + posUninformed.get(j));
				}*/
				
				Double sumProd = (double)0;
				Double sampleSize = (double)0;
				
				for(int j = 0; j < treeList.size(); j++) {
					int min = 0;
					int max = 0;
					double ratio = (double) 0;
					if(posInformed.get(j) <= posUninformed.get(j)) {
						min = posInformed.get(j);
						max = posUninformed.get(j);
					} else {
						max = posInformed.get(j);
						min = posUninformed.get(j);
					}
					ratio = (double)min/(double)max;
					sumProd = sumProd + (ratio * (posInformed.get(j) + posUninformed.get(j)));
					sampleSize = sampleSize + posInformed.get(j) + posUninformed.get(j);
				}
				SequenceFileWriter.writeAndAppendLine(path, "ParallelResults" + scriptfilename, commonList.get(i) + "\t" + sumProd + "\t" + sampleSize + "\t" + sumProd/sampleSize);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void extractPosteriorTruth(String path, String method) {
		String str;
		String ext = ".dis";
		String folder = "";
		if(method.equals("0")) {
			folder = "Posterior0/";
		} else if(method.equals("25")) {
			folder = "Posterior25/";
		} else if(method.equals("gelmanrubin")) {
			folder = "PosteriorGelman/";
		} else if(method == "geweke") {
			folder = "PosteriorGeweke/";
		} else if(method == "esslast") {
			folder = "PosteriorEssLast/";
		} else if(method == "essnormalized") {
			folder = "PosteriorEssNorm/";
		} else if(method == "essstandardized") {
			folder = "PosteriorEssStan/";
		}
		
		ArrayList<String> informedList = generateList(path + folder, ext);
		System.out.println(informedList.size());
		try {
			for(int i = 0; i < informedList.size(); i++) {
				BufferedReader bufferedreader = new BufferedReader(new FileReader(path + folder + informedList.get(i) + ext));
				
				int count = 0;
				while ((str = bufferedreader.readLine()) != null) {
					count++;
					if(count == 1) {
						String[] strArr = str.split("\t");
						SequenceFileWriter.writeAndAppendString(path + folder, "output.txt", informedList.get(i) + "\t" + strArr[1]);
					}
					
					if(count == 2) {
						String[] strArr = str.split("\t");
						SequenceFileWriter.writeAndAppendString(path + folder, "output.txt", "\t" + strArr[1] + "\t" + strArr[2]);
					}
					
					if(count == 3) {
						SequenceFileWriter.writeAndAppendString(path + folder, "output.txt", "\t" + str);
					}
				}
				SequenceFileWriter.writeAndAppendString(path + folder, "output.txt", "\n");
				bufferedreader.close();
				
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void generateScriptForNumericStatistics(String path, String method) {
		ArrayList<String> list = generateList(path + "MCMC/", ".mcmc");
		String str;
		int counter = 0;
		String scriptfilename = "";
		if(method.equals("0")) {
			scriptfilename = "StatisticsScriptZero";
		} else if(method.equals("25")) {
			scriptfilename = "StatisticsScriptTwentyFive";
		} else if(method.equals("gelmanrubin")) {
			scriptfilename = "StatisticsScriptGelman.sh";
		} else if(method == "geweke") {
			scriptfilename = "StatisticsScriptGeweke.sh";
		} else if(method == "noburnin") {
			scriptfilename = "StatisticsScriptNoBurnin.sh";
		} else if(method == "esslast") {
			scriptfilename = "StatisticsScriptEssLast.sh";
		} else if(method == "essnormalized") {
			scriptfilename = "StatisticsScriptEssNormalized.sh";
		} else if(method == "essstandardized") {
			scriptfilename = "StatisticsScriptEssStand.sh";
		}
			
		try {
			if(!method.equals("0") && !method.equals("25"))
				SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "#!/bin/bash\n");
			for(int i = 0; i < list.size(); i++) {
				try {
					if(method == "0") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b 0 -o " + list.get(i) + ".stat");
					} else if (method == "25") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b 2500 -o " + list.get(i) + ".stat25");
					} else {
						BufferedReader bufferedreader = new BufferedReader(new FileReader(path + "ConvergenceTest/" + list.get(i) + ".ct"));
						int count = 0;
						while ((str = bufferedreader.readLine()) != null) {
							count++;
							if(method == "gelmanrubin") {
								if(count == 9) {
									String[] strArr = str.split("Gelman_Rubin\": ");
			//						if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".statgr");
								}
							} else if(method == "geweke") {
								if(count == 7) {
									String[] strArr = str.split("Geweke\": ");
			//						if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".statge");
								}
							} else if(method == "noburnin") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b 0 -o " + list.get(i) + ".stat0");
							} else if(method == "25%") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b 12500 -o " + list.get(i) + ".stat25");
							} else if(method == "esslast") {
								if(count == 8) {
									String[] strArr = str.split("ESS\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".statel");
								}
							} else if(method == "essnormalized") {
								if(count == 11) {
									String[] strArr = str.split("Normalized ESS\": ");
									if(!strArr[1].equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1] + " -o " + list.get(i) + ".staten");
								}
							} else if(method == "essstandardized") {
								if(count == 10) {
									String[] strArr = str.split("Standardized ESS\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".states");
								}
							}
						}
						bufferedreader.close();
					}
				} catch (Exception e) {
					System.out.println("Error in writing script : " + e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("Error in writing script : " + e.getMessage());
		}
	}
	
	public static void generateScriptForConversionToMrBayes(String path) {
		ArrayList<String> list = generateList(path, ".fa");
			
		try {
			SequenceFileWriter.writeAndAppendLine(path, "MrBayesScript.sh", "#!/bin/bash\n");
			for(int i = 0; i < list.size(); i++) {
				try {
					SequenceFileWriter.writeAndAppendLine(path, "MrBayesScript.sh", "./fa2Informedmrbayes " + list.get(i) + ".fa " + list.get(i) + ".fas > " + list.get(i) + ".nex");
				} catch (Exception e) {
					System.out.println("Error in writing script : " + e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("Error in writing script : " + e.getMessage());
		}
	}
	
	public static void generateScriptForParallelAnalysisForInformedUninformed(String path, String method) {
		String str;
		int counter = 0;
		String scriptfilename = "";
		if(method.equals("0")) {
			scriptfilename = "ParallelScriptZero";
		} else if(method.equals("25")) {
			scriptfilename = "ParallelScriptTwentyFive";
		} else if(method.equals("gelmanrubin")) {
			scriptfilename = "ParallelScriptGelman.sh";
		} else if(method == "geweke") {
			scriptfilename = "ParallelScriptGeweke.sh";
		} else if(method == "noburnin") {
			scriptfilename = "ParallelScriptNoBurnin.sh";
		} else if(method == "esslast") {
			scriptfilename = "ParallelScriptEssLast.sh";
		} else if(method == "essnormalized") {
			scriptfilename = "ParallelScriptEssNormalized.sh";
		} else if(method == "essstandardized") {
			scriptfilename = "ParallelScriptEssStand.sh";
		}
		
		ArrayList<String> informedList = generateList(path + "Informed/MCMC/", ".mcmc");
		ArrayList<String> uninformedList = generateList(path + "Uninformed/MCMC/", ".mcmc");
		
		ArrayList<String> list = new ArrayList<String>();
		
		for(int i = 0; i < informedList.size(); i++) {
			for(int j = 0; j < uninformedList.size(); j++) {
				if(informedList.get(i).substring(8).equals(uninformedList.get(j))) {
					list.add(uninformedList.get(j));
					break;
				}
			}
		}
		
		try {
			if(!method.equals("0") && !method.equals("25"))
				SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "#!/bin/bash\n");
			for(int i = 0; i < list.size(); i++) {
				try {
					if(method == "0") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx2500m -Xms2000m -jar VMCMC-1.0.1-uber.jar ./Informed" + list.get(i) + ".mcmc ./" + list.get(i) + ".mcmc -b1 0 -b2 0 -o " + list.get(i) + ".par > " + list.get(i) + ".map");
					} else if (method == "25") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx2500m -Xms2000m -jar VMCMC-1.0.1-uber.jar ./Informed" + list.get(i) + ".mcmc ./" + list.get(i) + ".mcmc -b1 12500 -b2 12500 -o " + list.get(i) + ".par25 > " + list.get(i) + ".map25");
					} else {
						BufferedReader bufferedreader = new BufferedReader(new FileReader(path + "ConvergenceTest/" + list.get(i) + ".ct"));
						int count = 0;
						while ((str = bufferedreader.readLine()) != null) {
							count++;
							if(method == "gelmanrubin") {
								if(count == 9) {
									String[] strArr = str.split("Gelman_Rubin\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".statgr");
								}
							} else if(method == "geweke") {
								if(count == 7) {
									String[] strArr = str.split("Geweke\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".statge");
								}
							} else if(method == "noburnin") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b 0 -o " + list.get(i) + ".stat0");
							} else if(method == "25%") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b 12500 -o " + list.get(i) + ".stat25");
							} else if(method == "esslast") {
								if(count == 8) {
									String[] strArr = str.split("ESS\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".statel");
								}
							} else if(method == "essnormalized") {
								if(count == 11) {
									String[] strArr = str.split("Normalized ESS\": ");
									if(!strArr[1].equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1] + " -o " + list.get(i) + ".staten");
								}
							} else if(method == "essstandardized") {
								if(count == 10) {
									String[] strArr = str.split("Standardized ESS\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list.get(i) + ".states");
								}
							}
						}
						bufferedreader.close();
					}
				} catch (Exception e) {
					System.out.println("Error in writing script : " + e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("Error in writing script : " + e.getMessage());
		}
	}
	
	public static void generateScriptForParallelAnalysis(String path, String method) {
		String str;
		int counter = 0;
		String scriptfilename = "";
		if(method.equals("0")) {
			scriptfilename = "ParallelScriptZero";
		} else if(method.equals("25")) {
			scriptfilename = "ParallelScriptTwentyFive";
		} else if(method.equals("gelmanrubin")) {
			scriptfilename = "ParallelScriptGelman.sh";
		} else if(method == "geweke") {
			scriptfilename = "ParallelScriptGeweke.sh";
		} else if(method == "noburnin") {
			scriptfilename = "ParallelScriptNoBurnin.sh";
		} else if(method == "esslast") {
			scriptfilename = "ParallelScriptEssLast.sh";
		} else if(method == "essnormalized") {
			scriptfilename = "ParallelScriptEssNormalized.sh";
		} else if(method == "essstandardized") {
			scriptfilename = "ParallelScriptEssStand.sh";
		}
		
		ArrayList<String> list1 = generateList(path + "Run1/", ".mcmc");
		ArrayList<String> list2 = generateList(path + "Run2/", ".mcmc");
		
		try {
			if(!method.equals("0") && !method.equals("25"))
				SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "#!/bin/bash\n");
			for(int i = 0; i < list1.size(); i++) {
				try {
					if(method == "0") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx2500m -Xms2000m -jar VMCMC-1.0.1-uber.jar ./Run1/" + list1.get(i) + ".mcmc ./Run2/" + list2.get(i) + ".mcmc -b1 0 -b2 0 -o " + list1.get(i) + ".par > " + list1.get(i) + ".map");
					} else if (method == "25") {
						if(i % 200 == 0) {
							counter++;
							SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "#!/bin/bash\n");
						}
						SequenceFileWriter.writeAndAppendLine(path, scriptfilename + counter + ".sh", "java -Xmx2500m -Xms2000m -jar VMCMC-1.0.1-uber.jar ./Run1/" + list1.get(i) + ".mcmc ./Run2/" + list2.get(i) + ".mcmc -b1 2500 -b2 2500 -o " + list1.get(i) + ".par25 > " + list1.get(i) + ".map25");
					} else {
						BufferedReader bufferedreader = new BufferedReader(new FileReader(path + "ConvergenceTest/" + list1.get(i) + ".ct"));
						int count = 0;
						while ((str = bufferedreader.readLine()) != null) {
							count++;
							if(method == "gelmanrubin") {
								if(count == 9) {
									String[] strArr = str.split("Gelman_Rubin\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list1.get(i) + ".statgr");
								}
							} else if(method == "geweke") {
								if(count == 7) {
									String[] strArr = str.split("Geweke\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list1.get(i) + ".statge");
								}
							} else if(method == "noburnin") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b 0 -o " + list1.get(i) + ".stat0");
							} else if(method == "25%") {
								SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b 12500 -o " + list1.get(i) + ".stat25");
							} else if(method == "esslast") {
								if(count == 8) {
									String[] strArr = str.split("ESS\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list1.get(i) + ".statel");
								}
							} else if(method == "essnormalized") {
								if(count == 11) {
									String[] strArr = str.split("Normalized ESS\": ");
									if(!strArr[1].equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b " + strArr[1] + " -o " + list1.get(i) + ".staten");
								}
							} else if(method == "essstandardized") {
								if(count == 10) {
									String[] strArr = str.split("Standardized ESS\": ");
									if(!(strArr[1].substring(0, strArr[1].length() - 1)).equals("0"))
										SequenceFileWriter.writeAndAppendLine(path, scriptfilename, "java -Xmx1500m -Xms1000m -jar VMCMC-1.0.1-uber.jar " + list1.get(i) + ".mcmc -s -b " + strArr[1].substring(0, strArr[1].length() - 1) + " -o " + list1.get(i) + ".states");
								}
							}
						}
						bufferedreader.close();
					}
				} catch (Exception e) {
					System.out.println("Error in writing script : " + e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("Error in writing script : " + e.getMessage());
		}
	}
	
	public static void generateScriptForBurninEstimation(String path) {
		ArrayList<String> list = generateList(path + "MCMC/", ".mcmc");
		int count = 0;
		try {
			for(int i = 0; i < list.size(); i++) {
				if(i % 200 == 0) {
					count++;
					SequenceFileWriter.writeAndAppendLine(path, "ConvergenceScript" + count + ".sh", "#!/bin/bash\n");
				}
				SequenceFileWriter.writeAndAppendLine(path, "ConvergenceScript" + count + ".sh", "java -Xmx1000m -Xms800m -jar VMCMC-1.0.1-uber.jar " + list.get(i) + ".mcmc -ct -o " + list.get(i) + ".ct");
			}
		} catch (Exception e) {
			System.out.println("Error in writing script : " + e.getMessage());
		}
	}
	
	public static ArrayList<String> generateList(String path, String extension) {
		try {
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			int numberofFiles = listOfFiles.length;
			ArrayList<String> list = new ArrayList<String>();

			for(int i = 0; i < numberofFiles; i++) {
				if(listOfFiles[i].getName().endsWith(extension))
					list.add(listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length()-extension.length()));
			}
			return list;
		} catch (Exception e) {
			System.out.println("Error in list finder : " + e.getMessage());
			System.exit(-1);
		}
		return null;
	}
}
